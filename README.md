# TI-Schematics

Welcome to the open repository for schematics related to the [TI-99/4A](https://en.wikipedia.org/wiki/Texas_Instruments_TI-99/4A) System. 
If you want to have a closer look, please visit the [Wiki pages](../../wiki/Home).


## What is this repository for?

The goal of this repository is to collect all digitally captured [TI-99/4A](https://en.wikipedia.org/wiki/Texas_Instruments_TI-99/4A) System related schematics. This includes the schematics for the console and all peripheral equipment like speech synthesizer or the expansion cards in the PE-System. Also self made, home brew hardware or third party hardware can be achieved here.

The motivation of starting this repository is that the scans/copies of the well known printed schemata on [WHTech FTP-Server](ftp://ftp.whtech.com/) are all in an bad condition and are not very useful. They contain errors, are badly readable and you can't do nothing else with them than watching them.  
So with this new basis of digitally captured schematics we have new options to develop new hardware for the TI-99/4A, have better chances to repair our old systems or we can edit the sheets to correct errors and do some other enhancements. If you want, you can make your own Boards and build your own system from scratch. We can make branches where we can work on a true 16 bit machine or with an other branch we can realize the ultimative dream machine, or we can build schematics where the [F18A](http://codehackcreate.com/archives/30) (see [Code | Hack | Create](http://codehackcreate.com)) is natively integrated, for example.  
For me, someday I will have my own modular [ECB](https://en.wikipedia.org/wiki/Europe_Card_Bus) or [VME](https://en.wikipedia.org/wiki/VMEbus) based TI-99/4A in a 19" rack.

All schematics, PCBs, footprints or libraries (etc.) are (and should be) created/edited with the actual version of [KiCad EDA](http://kicad-pcb.org/), a cross platform and Open Source Electronics Design Automation Suite.

If you want to contribute some schematics or PCB designs, you are welcome.


## License

The works in this repository are published under different copyrights and are released for use by different licenses.  
For more information, please see the respective readme files.  
A quick overview for the following projects:

- [EPROMer512](https://bitbucket.org/hackmac/ti-schematics/wiki/EPROMer512) ([Source](https://bitbucket.org/hackmac/ti-schematics/src/master/EPROMer512/)) © 1988-2018 by Rürgen Reimer and [Henrik Wedekind (aka. hackmac)](https://bitbucket.org/hackmac/) is licensed under [CC BY-NC-ND 4.0](http://creativecommons.org/licenses/by-nc-nd/4.0/)
- [TI-99/4A](https://bitbucket.org/hackmac/ti-schematics/wiki/TI99-4A) ([Source](https://bitbucket.org/hackmac/ti-schematics/src/master/TI-99_4A/)) and [TI-Expansion-System](https://bitbucket.org/hackmac/ti-schematics/wiki/TI-PEB) ([Source](https://bitbucket.org/hackmac/ti-schematics/src/master/TI-Expansion-System/)) © by [Texas Instruments](https://www.ti.com/) and others (while referencing different documents for information)  
(This work may have limited or "hybrid" public domain status for various reasons. The [CC](http://creativecommons.org) does not currently recommend the public domain mark for works with limited, mixed public domain status.)
See also: [TI-99 related Specification Documents and Software License to work with the System Source Code](https://e2e.ti.com/support/interface-group/interface/f/interface-forum/667878/ti-99-related-specification-documents-and-software-license-to-work-with-the-system-source-code?keyMatch=TI-99%2f4A)
- [libraries](https://bitbucket.org/hackmac/ti-schematics/src/master/libraries/) and [modules](https://bitbucket.org/hackmac/ti-schematics/src/master/modules/) © 2018 by [Henrik Wedekind (aka. hackmac)](https://bitbucket.org/hackmac/) is licensed under [CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/)
