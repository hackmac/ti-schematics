# EPROMer512

It was one of my first greater hardware projects, back in 1988.
The hardware component was created by Jürgen Reimer and [Henrik Wedekind (aka. hackmac)](https://bitbucket.org/hackmac/). Both where sitting in front of the TI-99/4A and handwired each track on a two layered PCB, which took several month of work, and after that, printed the PCB out on an EPSON FX-80 impact printer to make a transparency film from this print, where the PCB can be produced from.

Due to the 30th anniversary, I digitized the old papers and publish these documents. You can see some pictures from the produced prototype as a result of the work back the time at the bottom of the [Wiki page](https://bitbucket.org/hackmac/ti-schematics/wiki/EPROMer512).

All schematics, PCBs, footprints or libraries (etc.) are (and should be) created/edited with the actual version of [KiCad EDA](http://kicad-pcb.org/), a cross platform and Open Source Electronics Design Automation Suite.


## License ![](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc-nd.eu.svg)

[EPROMer512](https://bitbucket.org/hackmac/ti-schematics/wiki/EPROMer512) ([Source](https://bitbucket.org/hackmac/ti-schematics/src/master/EPROMer512/)) © 1988-2018 by Rürgen Reimer and [Henrik Wedekind (aka. hackmac)](https://bitbucket.org/hackmac/) is licensed under [CC BY-NC-ND 4.0](http://creativecommons.org/licenses/by-nc-nd/4.0/)
