EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:conn_01x44
LIBS:FlexCable-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Flex Cable Schematic Diagram"
Date "2015-10-12"
Rev "1"
Comp "hackmac"
Comment1 "Peripheral Expansion Unit End of Cable"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X44 P2
U 1 1 561D3F26
P 1600 4950
F 0 "P2" H 1600 7200 50  0000 C CNN
F 1 "CONN_01X44" V 1700 4950 50  0000 C CNN
F 2 "" H 1600 5150 60  0000 C CNN
F 3 "" H 1600 5150 60  0000 C CNN
	1    1600 4950
	-1   0    0    -1  
$EndComp
$Comp
L CONN_02X30 P1
U 1 1 561D451B
P 9850 3650
F 0 "P1" H 9850 5200 50  0000 C CNN
F 1 "CONN_02X30" V 9850 3650 50  0000 C CNN
F 2 "" H 9850 3150 60  0000 C CNN
F 3 "" H 9850 3150 60  0000 C CNN
	1    9850 3650
	1    0    0    -1  
$EndComp
Entry Wire Line
	10400 2300 10500 2200
Entry Wire Line
	10400 2400 10500 2300
Entry Wire Line
	10400 2600 10500 2500
Entry Wire Line
	10400 3200 10500 3100
Entry Wire Line
	10400 3300 10500 3200
Entry Wire Line
	10400 3400 10500 3300
Entry Wire Line
	10400 3500 10500 3400
Entry Wire Line
	10400 3600 10500 3500
Entry Wire Line
	10400 3700 10500 3600
Entry Wire Line
	10400 3800 10500 3700
Entry Wire Line
	10400 3900 10500 3800
Entry Wire Line
	10400 4000 10500 3900
Entry Wire Line
	10400 4100 10500 4000
Entry Wire Line
	10400 4200 10500 4100
Entry Wire Line
	10400 4300 10500 4200
Entry Wire Line
	10400 4600 10500 4500
Entry Wire Line
	10400 4700 10500 4600
Entry Wire Line
	10400 4800 10500 4700
Entry Wire Line
	10400 4900 10500 4800
Entry Wire Line
	9200 2600 9300 2700
Entry Wire Line
	9200 2900 9300 3000
Entry Wire Line
	9200 3000 9300 3100
Entry Wire Line
	9200 3100 9300 3200
Entry Wire Line
	9200 3200 9300 3300
Entry Wire Line
	9200 3300 9300 3400
Entry Wire Line
	9200 3500 9300 3600
Entry Wire Line
	9200 3600 9300 3700
Entry Wire Line
	9200 3700 9300 3800
Entry Wire Line
	9200 3800 9300 3900
Entry Wire Line
	9200 3900 9300 4000
Entry Wire Line
	9200 4000 9300 4100
Entry Wire Line
	9200 4100 9300 4200
Entry Wire Line
	9200 4200 9300 4300
Entry Wire Line
	9200 4600 9300 4700
Entry Wire Line
	9200 4800 9300 4900
Entry Wire Line
	10400 2200 10500 2100
Text Label 10100 2200 0    60   ~ 0
Unreg.+8V
$Comp
L GND #PWR?
U 1 1 561DAB70
P 9100 2350
F 0 "#PWR?" H 9100 2100 50  0001 C CNN
F 1 "GND" H 9100 2200 50  0000 C CNN
F 2 "" H 9100 2350 60  0000 C CNN
F 3 "" H 9100 2350 60  0000 C CNN
	1    9100 2350
	1    0    0    -1  
$EndComp
Entry Wire Line
	10400 5000 10500 4900
Entry Wire Line
	10400 5100 10500 5000
Text Label 10100 5000 0    60   ~ 0
Unreg.-16V
Text Label 10100 5100 0    60   ~ 0
Unreg.+16V
Text Label 10100 2500 0    60   ~ 0
SCLK
Text Label 10100 2800 0    60   ~ 0
IAQHA
Text Label 10100 3000 0    60   ~ 0
~LOAD
Text Label 9600 2600 2    60   ~ 0
~LCP
Text Label 10100 2300 0    60   ~ 0
READY.A
Text Label 10100 2400 0    60   ~ 0
~RESET
Text Label 10100 2600 0    60   ~ 0
AUDIO
Text Label 10100 2700 0    60   ~ 0
PCBEN
$Comp
L R R9
U 1 1 561DBE47
P 10700 2700
F 0 "R9" V 10700 2700 50  0000 C CNN
F 1 "47" V 10650 2850 50  0000 C CNN
F 2 "" V 10630 2700 30  0000 C CNN
F 3 "" H 10700 2700 30  0000 C CNN
	1    10700 2700
	0    1    1    0   
$EndComp
$Comp
L R R12
U 1 1 561DBE98
P 10700 2900
F 0 "R12" V 10700 2900 50  0000 C CNN
F 1 "47" V 10650 3050 50  0000 C CNN
F 2 "" V 10630 2900 30  0000 C CNN
F 3 "" H 10700 2900 30  0000 C CNN
	1    10700 2900
	0    1    1    0   
$EndComp
$Comp
L R R14
U 1 1 561DBEB6
P 10700 4350
F 0 "R14" V 10700 4350 50  0000 C CNN
F 1 "47" V 10650 4500 50  0000 C CNN
F 2 "" V 10630 4350 30  0000 C CNN
F 3 "" H 10700 4350 30  0000 C CNN
	1    10700 4350
	0    1    1    0   
$EndComp
$Comp
L R R13
U 1 1 561DBEED
P 9000 4400
F 0 "R13" V 9000 4400 50  0000 C CNN
F 1 "47" V 8950 4250 50  0000 C CNN
F 2 "" V 8930 4400 30  0000 C CNN
F 3 "" H 9000 4400 30  0000 C CNN
	1    9000 4400
	0    1    1    0   
$EndComp
$Comp
L R R11
U 1 1 561DBF0D
P 9000 2850
F 0 "R11" V 9000 2850 50  0000 C CNN
F 1 "47" V 8950 2700 50  0000 C CNN
F 2 "" V 8930 2850 30  0000 C CNN
F 3 "" H 9000 2850 30  0000 C CNN
	1    9000 2850
	0    1    1    0   
$EndComp
$Comp
L R R10
U 1 1 561DBF38
P 9000 2750
F 0 "R10" V 9000 2750 50  0000 C CNN
F 1 "47" V 8950 2600 50  0000 C CNN
F 2 "" V 8930 2750 30  0000 C CNN
F 3 "" H 9000 2750 30  0000 C CNN
	1    9000 2750
	0    1    1    0   
$EndComp
$Comp
L R R15
U 1 1 561DBF5B
P 10700 4450
F 0 "R15" V 10700 4450 50  0000 C CNN
F 1 "47" V 10650 4600 50  0000 C CNN
F 2 "" V 10630 4450 30  0000 C CNN
F 3 "" H 10700 4450 30  0000 C CNN
	1    10700 4450
	0    1    1    0   
$EndComp
$Comp
L 7805 Q1
U 1 1 561DD365
P 8250 2100
F 0 "Q1" H 8400 1904 60  0000 C CNN
F 1 "78M05" H 8250 2300 60  0000 C CNN
F 2 "" H 8250 2100 60  0000 C CNN
F 3 "" H 8250 2100 60  0000 C CNN
	1    8250 2100
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 561DD742
P 8250 2450
F 0 "#PWR?" H 8250 2200 50  0001 C CNN
F 1 "GND" H 8250 2300 50  0000 C CNN
F 2 "" H 8250 2450 60  0000 C CNN
F 3 "" H 8250 2450 60  0000 C CNN
	1    8250 2450
	1    0    0    -1  
$EndComp
$Comp
L C_Small C9
U 1 1 561DD9AA
P 8700 2200
F 0 "C9" H 8710 2270 50  0000 L CNN
F 1 "22µ" H 8710 2120 50  0000 L CNN
F 2 "" H 8700 2200 60  0000 C CNN
F 3 "" H 8700 2200 60  0000 C CNN
	1    8700 2200
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 561DD9D5
P 8850 2200
F 0 "C1" H 8860 2270 50  0000 L CNN
F 1 "1" H 8860 2120 50  0000 L CNN
F 2 "" H 8850 2200 60  0000 C CNN
F 3 "" H 8850 2200 60  0000 C CNN
	1    8850 2200
	1    0    0    -1  
$EndComp
$Comp
L C_Small C8
U 1 1 561DDA01
P 7800 2200
F 0 "C8" H 7810 2270 50  0000 L CNN
F 1 "22µ" H 7810 2120 50  0000 L CNN
F 2 "" H 7800 2200 60  0000 C CNN
F 3 "" H 7800 2200 60  0000 C CNN
	1    7800 2200
	1    0    0    -1  
$EndComp
$Comp
L C_Small C2
U 1 1 561DDA2D
P 7650 2200
F 0 "C2" H 7660 2270 50  0000 L CNN
F 1 "1" H 7660 2120 50  0000 L CNN
F 2 "" H 7650 2200 60  0000 C CNN
F 3 "" H 7650 2200 60  0000 C CNN
	1    7650 2200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 561DF331
P 10650 3150
F 0 "#PWR?" H 10650 2900 50  0001 C CNN
F 1 "GND" H 10650 3000 50  0000 C CNN
F 2 "" H 10650 3150 60  0000 C CNN
F 3 "" H 10650 3150 60  0000 C CNN
	1    10650 3150
	1    0    0    -1  
$EndComp
Text Label 10100 3200 0    60   ~ 0
D6.A
Text Label 10100 3300 0    60   ~ 0
D4.A
Text Label 10100 3400 0    60   ~ 0
D2.A
Text Label 10100 3500 0    60   ~ 0
D0.A
Text Label 10100 3600 0    60   ~ 0
A15/CRUOUT.A
Text Label 10100 3700 0    60   ~ 0
A13.A
Text Label 10100 3800 0    60   ~ 0
A11.A
Text Label 10100 3900 0    60   ~ 0
A9.A
Text Label 10100 4000 0    60   ~ 0
A7.A
Text Label 10100 4100 0    60   ~ 0
A5.A
Text Label 10100 4200 0    60   ~ 0
A3.A
Text Label 10100 4300 0    60   ~ 0
A1.A
Text Label 10100 4400 0    60   ~ 0
AMA.A
Text Label 10100 4500 0    60   ~ 0
AMC.A
Text Label 10100 4600 0    60   ~ 0
~Ø3.A
Text Label 10100 4700 0    60   ~ 0
DBIN.A
Text Label 10100 4800 0    60   ~ 0
~WE.A
Text Label 10100 4900 0    60   ~ 0
~MEMEN.A
Text Label 9600 2700 2    60   ~ 0
~RDBENA
Text Label 9600 2800 2    60   ~ 0
~HOLD
Text Label 9600 2900 2    60   ~ 0
~SENILA
Text Label 9600 3000 2    60   ~ 0
~INTA
Text Label 10100 2900 0    60   ~ 0
~SENILB
Text Label 9600 3100 2    60   ~ 0
D7.A
Text Label 9600 3200 2    60   ~ 0
D5.A
Text Label 9600 3300 2    60   ~ 0
D3.A
Text Label 9600 3400 2    60   ~ 0
D1.A
Text Label 9600 3600 2    60   ~ 0
A14.A
Text Label 9600 3700 2    60   ~ 0
A12.A
Text Label 9600 3800 2    60   ~ 0
A10.A
Text Label 9600 3900 2    60   ~ 0
A8.A
Text Label 9600 4000 2    60   ~ 0
A6.A
Text Label 9600 4100 2    60   ~ 0
A4.A
Text Label 9600 4200 2    60   ~ 0
A2.A
Text Label 9600 4300 2    60   ~ 0
A0.A
Text Label 9600 4400 2    60   ~ 0
AMB.A
Text Label 9600 4700 2    60   ~ 0
~CRUCLK.A
Text Label 9600 4900 2    60   ~ 0
CRUIN
Text HLabel 1400 2800 0    60   Output ~ 0
FC_1
Text HLabel 1400 2900 0    60   Input ~ 0
FC_2
Text HLabel 1400 3000 0    60   UnSpc ~ 0
FC_3
Text HLabel 1400 3100 0    60   Input ~ 0
FC_4
Text HLabel 1400 3200 0    60   Input ~ 0
FC_5
Text HLabel 1400 3300 0    60   Input ~ 0
FC_6
Text HLabel 1400 3400 0    60   UnSpc ~ 0
FC_7
Text HLabel 1400 3500 0    60   Input ~ 0
FC_8
Text HLabel 1400 3600 0    60   UnSpc ~ 0
FC_9
Text HLabel 1400 3700 0    60   Input ~ 0
FC_10
Text HLabel 1400 3800 0    60   Input ~ 0
FC_11
Text HLabel 1400 3900 0    60   Input ~ 0
FC_12
Text HLabel 1400 4000 0    60   Input ~ 0
FC_13
Text HLabel 1400 4100 0    60   Input ~ 0
FC_14
Text HLabel 1400 4200 0    60   Input ~ 0
FC_15
Text HLabel 1400 4300 0    60   Input ~ 0
FC_16
Text HLabel 1400 4400 0    60   Input ~ 0
FC_17
Text HLabel 1400 4500 0    60   Input ~ 0
FC_18
Text HLabel 1400 4600 0    60   Input ~ 0
FC_19
Text HLabel 1400 4700 0    60   Input ~ 0
FC_20
Text HLabel 1400 4800 0    60   Input ~ 0
FC_21
Text HLabel 1400 4900 0    60   Input ~ 0
FC_22
Text HLabel 1400 5000 0    60   Input ~ 0
FC_23
Text HLabel 1400 5100 0    60   Input ~ 0
FC_24
Text HLabel 1400 5200 0    60   UnSpc ~ 0
FC_25
Text HLabel 1400 5300 0    60   Input ~ 0
FC_26
Text HLabel 1400 5400 0    60   UnSpc ~ 0
FC_27
Text HLabel 1400 5500 0    60   BiDi ~ 0
FC_28
Text HLabel 1400 5600 0    60   BiDi ~ 0
FC_29
Text HLabel 1400 5700 0    60   BiDi ~ 0
FC_30
Text HLabel 1400 5800 0    60   BiDi ~ 0
FC_31
Text HLabel 1400 5900 0    60   UnSpc ~ 0
FC_32
Text HLabel 1400 6000 0    60   BiDi ~ 0
FC_33
Text HLabel 1400 6100 0    60   BiDi ~ 0
FC_34
Text HLabel 1400 6200 0    60   BiDi ~ 0
FC_35
Text HLabel 1400 6300 0    60   BiDi ~ 0
FC_36
Text HLabel 1400 6400 0    60   UnSpc ~ 0
FC_37
Text HLabel 1400 6500 0    60   Input ~ 0
FC_38
Text HLabel 1400 6600 0    60   Output ~ 0
FC_39
Text HLabel 1400 6700 0    60   Input ~ 0
FC_40
Text HLabel 1400 6800 0    60   Output ~ 0
FC_41
Text HLabel 1400 6900 0    60   UnSpc ~ 0
FC_42
Text HLabel 1400 7000 0    60   Output ~ 0
FC_43
Text HLabel 1400 7100 0    60   UnSpc ~ 0
FC_44
Text Notes 1400 7400 0    60   ~ 0
FLEXCABLE\nCONNECTOR
Text Notes 9600 5650 0    60   ~ 0
PERIPHERAL\nEXPANSION\nBUS
Entry Wire Line
	2350 2800 2450 2700
Entry Wire Line
	2350 2900 2450 2800
Entry Wire Line
	2350 3100 2450 3000
Entry Wire Line
	2350 3200 2450 3100
Entry Wire Line
	2350 3300 2450 3200
Entry Wire Line
	2350 3500 2450 3400
Entry Wire Line
	2350 3700 2450 3600
Entry Wire Line
	2350 3800 2450 3700
Entry Wire Line
	2350 3900 2450 3800
Entry Wire Line
	2350 4000 2450 3900
Entry Wire Line
	2350 4100 2450 4000
Entry Wire Line
	2350 4200 2450 4100
Entry Wire Line
	2350 4300 2450 4200
Entry Wire Line
	2350 4400 2450 4300
Entry Wire Line
	2350 4500 2450 4400
Entry Wire Line
	2350 4600 2450 4500
Entry Wire Line
	2350 4700 2450 4600
Entry Wire Line
	2350 4800 2450 4700
Entry Wire Line
	2350 4900 2450 4800
Entry Wire Line
	2350 5000 2450 4900
Entry Wire Line
	2350 5100 2450 5000
Entry Wire Line
	2350 5300 2450 5200
Entry Wire Line
	2350 5500 2450 5400
Entry Wire Line
	2350 5600 2450 5500
Entry Wire Line
	2350 5700 2450 5600
Entry Wire Line
	2350 5800 2450 5700
Entry Wire Line
	2350 6000 2450 5900
Entry Wire Line
	2350 6100 2450 6000
Entry Wire Line
	2350 6200 2450 6100
Entry Wire Line
	2350 6300 2450 6200
Entry Wire Line
	2350 6500 2450 6400
Entry Wire Line
	2350 6600 2450 6500
Entry Wire Line
	2350 6700 2450 6600
Entry Wire Line
	2350 6800 2450 6700
Entry Wire Line
	2350 7000 2450 6900
Text Label 1850 3700 0    60   ~ 0
A0
Text Label 1850 3800 0    60   ~ 0
A1
Text Label 1850 3900 0    60   ~ 0
A2
Text Label 1850 4000 0    60   ~ 0
A3
Text Label 1850 4100 0    60   ~ 0
A4
Text Label 1850 4200 0    60   ~ 0
A5
Text Label 1850 4300 0    60   ~ 0
A6
Text Label 1850 4400 0    60   ~ 0
A7
Text Label 1850 4500 0    60   ~ 0
A8
Text Label 1850 4600 0    60   ~ 0
A9
Text Label 1850 4700 0    60   ~ 0
A10
Text Label 1850 4800 0    60   ~ 0
A11
Text Label 1850 4900 0    60   ~ 0
A12
Text Label 1850 5000 0    60   ~ 0
A13
Text Label 1850 5100 0    60   ~ 0
A14
Text Label 1850 5300 0    60   ~ 0
A15/CRUOUT
Text Label 1850 5500 0    60   ~ 0
D0
Text Label 1850 5600 0    60   ~ 0
D1
Text Label 1850 5700 0    60   ~ 0
D2
Text Label 1850 5800 0    60   ~ 0
D3
Text Label 1850 6000 0    60   ~ 0
D4
Text Label 1850 6100 0    60   ~ 0
D5
Text Label 1850 6200 0    60   ~ 0
D6
Text Label 1850 6300 0    60   ~ 0
D7
Text Label 1850 2800 0    60   ~ 0
CRUIN
Text Label 1850 2900 0    60   ~ 0
~MEMEN
Text Label 1850 3100 0    60   ~ 0
~WE
Text Label 1850 3200 0    60   ~ 0
~CRUCLK
Text Label 1850 3300 0    60   ~ 0
DBIN
Text Label 1850 3500 0    60   ~ 0
~Ø3
Text Label 1850 6500 0    60   ~ 0
~RESET
Text Label 1850 6600 0    60   ~ 0
AUDION
Text Label 1850 6700 0    60   ~ 0
READY
Text Label 1850 6800 0    60   ~ 0
~EXTINT
Text Label 1850 7000 0    60   ~ 0
~RDBENA
$Comp
L 74LS244 U1
U 1 1 561E714A
P 3800 3300
F 0 "U1" H 3850 3100 60  0000 C CNN
F 1 "74LS244" H 3900 2900 60  0000 C CNN
F 2 "" H 3800 3300 60  0000 C CNN
F 3 "" H 3800 3300 60  0000 C CNN
	1    3800 3300
	1    0    0    -1  
$EndComp
$Comp
L 74LS244 U2
U 1 1 561E71D4
P 3800 4600
F 0 "U2" H 3850 4400 60  0000 C CNN
F 1 "74LS244" H 3900 4200 60  0000 C CNN
F 2 "" H 3800 4600 60  0000 C CNN
F 3 "" H 3800 4600 60  0000 C CNN
	1    3800 4600
	1    0    0    -1  
$EndComp
$Comp
L 74LS244 U3
U 1 1 561E7214
P 3800 5900
F 0 "U3" H 3850 5700 60  0000 C CNN
F 1 "74LS244" H 3900 5500 60  0000 C CNN
F 2 "" H 3800 5900 60  0000 C CNN
F 3 "" H 3800 5900 60  0000 C CNN
	1    3800 5900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 561E7FF9
P 3050 3850
F 0 "#PWR?" H 3050 3600 50  0001 C CNN
F 1 "GND" H 3050 3700 50  0000 C CNN
F 2 "" H 3050 3850 60  0000 C CNN
F 3 "" H 3050 3850 60  0000 C CNN
	1    3050 3850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 561E802D
P 3050 5150
F 0 "#PWR?" H 3050 4900 50  0001 C CNN
F 1 "GND" H 3050 5000 50  0000 C CNN
F 2 "" H 3050 5150 60  0000 C CNN
F 3 "" H 3050 5150 60  0000 C CNN
	1    3050 5150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 561E805F
P 3050 6450
F 0 "#PWR?" H 3050 6200 50  0001 C CNN
F 1 "GND" H 3050 6300 50  0000 C CNN
F 2 "" H 3050 6450 60  0000 C CNN
F 3 "" H 3050 6450 60  0000 C CNN
	1    3050 6450
	1    0    0    -1  
$EndComp
Text Label 3100 2800 2    60   ~ 0
DBIN
Text Label 3100 2900 2    60   ~ 0
A2
Text Label 3100 3000 2    60   ~ 0
A1
Text Label 3100 3100 2    60   ~ 0
A0
Text Label 3100 3200 2    60   ~ 0
~MEMEN
Text Label 3100 3300 2    60   ~ 0
A3
Text Label 3100 3400 2    60   ~ 0
A4
Text Label 3100 3500 2    60   ~ 0
A5
Text Label 3100 4100 2    60   ~ 0
A6
Text Label 3100 4200 2    60   ~ 0
A7
Text Label 3100 4300 2    60   ~ 0
A8
Text Label 3100 4400 2    60   ~ 0
A9
Text Label 3100 4500 2    60   ~ 0
A10
Text Label 3100 4600 2    60   ~ 0
A11
Text Label 3100 4700 2    60   ~ 0
A12
Text Label 3100 4800 2    60   ~ 0
A13
Text Label 3100 5400 2    60   ~ 0
A14
Text Label 3100 5500 2    60   ~ 0
A15/CRUOUT
Text Label 3100 5600 2    60   ~ 0
~WE
Text Label 3100 5700 2    60   ~ 0
~CRUCLK
Text Label 2800 6750 0    60   ~ 0
~RDBENA
Text Label 3100 5800 2    60   ~ 0
~RESET
Text Label 3100 5900 2    60   ~ 0
~Ø3
Entry Wire Line
	2450 6850 2550 6750
$Comp
L R R17
U 1 1 561EA943
P 3800 6750
F 0 "R17" V 3800 6750 50  0000 C CNN
F 1 "47" V 3850 6900 50  0000 C CNN
F 2 "" V 3730 6750 30  0000 C CNN
F 3 "" H 3800 6750 30  0000 C CNN
	1    3800 6750
	0    1    1    0   
$EndComp
$Comp
L R R16
U 1 1 561EAEB0
P 4350 6450
F 0 "R16" V 4430 6450 50  0000 C CNN
F 1 "1k" V 4350 6450 50  0000 C CNN
F 2 "" V 4280 6450 30  0000 C CNN
F 3 "" H 4350 6450 30  0000 C CNN
	1    4350 6450
	1    0    0    -1  
$EndComp
$Comp
L 74LS245 U4
U 1 1 561EB56C
P 3800 2000
F 0 "U4" H 3850 1800 60  0000 L BNN
F 1 "74LS245" H 3750 1650 60  0000 L TNN
F 2 "" H 3800 2000 60  0000 C CNN
F 3 "" H 3800 2000 60  0000 C CNN
	1    3800 2000
	-1   0    0    -1  
$EndComp
Entry Wire Line
	2450 2300 2550 2200
Entry Wire Line
	2450 1600 2550 1500
Entry Wire Line
	2450 1700 2550 1600
Entry Wire Line
	2450 1800 2550 1700
Entry Wire Line
	2450 1900 2550 1800
Entry Wire Line
	2450 2000 2550 1900
Entry Wire Line
	2450 2100 2550 2000
Entry Wire Line
	2450 2200 2550 2100
Text Label 2750 1500 2    60   ~ 0
D0
Text Label 2750 1600 2    60   ~ 0
D1
Text Label 2750 1700 2    60   ~ 0
D2
Text Label 2750 1800 2    60   ~ 0
D3
Text Label 2750 1900 2    60   ~ 0
D4
Text Label 2750 2000 2    60   ~ 0
D5
Text Label 2750 2100 2    60   ~ 0
D6
Text Label 2750 2200 2    60   ~ 0
D7
Entry Wire Line
	4950 1500 5050 1400
Entry Wire Line
	4950 1600 5050 1500
Entry Wire Line
	4950 1700 5050 1600
Entry Wire Line
	4950 1800 5050 1700
Entry Wire Line
	4950 1900 5050 1800
Entry Wire Line
	4950 2000 5050 1900
Entry Wire Line
	4950 2100 5050 2000
Entry Wire Line
	4950 2200 5050 2100
Entry Wire Line
	4950 2800 5050 2700
Entry Wire Line
	4950 2900 5050 2800
Entry Wire Line
	4950 3000 5050 2900
Entry Wire Line
	4950 3100 5050 3000
Entry Wire Line
	4950 3200 5050 3100
Entry Wire Line
	4950 3300 5050 3200
Entry Wire Line
	4950 3400 5050 3300
Entry Wire Line
	4950 3500 5050 3400
Entry Wire Line
	4950 4100 5050 4000
Entry Wire Line
	4950 4200 5050 4100
Entry Wire Line
	4950 4300 5050 4200
Entry Wire Line
	4950 4400 5050 4300
Entry Wire Line
	4950 4500 5050 4400
Entry Wire Line
	4950 4600 5050 4500
Entry Wire Line
	4950 4700 5050 4600
Entry Wire Line
	4950 4800 5050 4700
Entry Wire Line
	4950 5400 5050 5300
Entry Wire Line
	4950 5500 5050 5400
Entry Wire Line
	4950 5600 5050 5500
Entry Wire Line
	4950 5700 5050 5600
Entry Wire Line
	4950 5800 5050 5700
Entry Wire Line
	4950 5900 5050 5800
Entry Wire Line
	2450 2900 2550 2800
Entry Wire Line
	2450 3000 2550 2900
Entry Wire Line
	2450 3100 2550 3000
Entry Wire Line
	2450 3200 2550 3100
Entry Wire Line
	2450 3300 2550 3200
Entry Wire Line
	2450 3400 2550 3300
Entry Wire Line
	2450 3500 2550 3400
Entry Wire Line
	2450 3600 2550 3500
Entry Wire Line
	2450 4200 2550 4100
Entry Wire Line
	2450 4300 2550 4200
Entry Wire Line
	2450 4400 2550 4300
Entry Wire Line
	2450 4500 2550 4400
Entry Wire Line
	2450 4600 2550 4500
Entry Wire Line
	2450 4700 2550 4600
Entry Wire Line
	2450 4800 2550 4700
Entry Wire Line
	2450 4900 2550 4800
Entry Wire Line
	2450 5500 2550 5400
Entry Wire Line
	2450 5600 2550 5500
Entry Wire Line
	2450 5700 2550 5600
Entry Wire Line
	2450 5800 2550 5700
Entry Wire Line
	2450 5900 2550 5800
Entry Wire Line
	2450 6000 2550 5900
$Comp
L R R1
U 1 1 561EDFB0
P 2900 1500
F 0 "R1" V 2900 1500 50  0000 C CNN
F 1 "47" V 2850 1650 50  0000 C CNN
F 2 "" V 2830 1500 30  0000 C CNN
F 3 "" H 2900 1500 30  0000 C CNN
	1    2900 1500
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 561EE036
P 2900 1600
F 0 "R2" V 2900 1600 50  0000 C CNN
F 1 "47" V 2850 1750 50  0000 C CNN
F 2 "" V 2830 1600 30  0000 C CNN
F 3 "" H 2900 1600 30  0000 C CNN
	1    2900 1600
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 561EE07E
P 2900 1700
F 0 "R3" V 2900 1700 50  0000 C CNN
F 1 "47" V 2850 1850 50  0000 C CNN
F 2 "" V 2830 1700 30  0000 C CNN
F 3 "" H 2900 1700 30  0000 C CNN
	1    2900 1700
	0    1    1    0   
$EndComp
$Comp
L R R4
U 1 1 561EE0BF
P 2900 1800
F 0 "R4" V 2900 1800 50  0000 C CNN
F 1 "47" V 2850 1950 50  0000 C CNN
F 2 "" V 2830 1800 30  0000 C CNN
F 3 "" H 2900 1800 30  0000 C CNN
	1    2900 1800
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 561EE102
P 2900 1900
F 0 "R5" V 2900 1900 50  0000 C CNN
F 1 "47" V 2850 2050 50  0000 C CNN
F 2 "" V 2830 1900 30  0000 C CNN
F 3 "" H 2900 1900 30  0000 C CNN
	1    2900 1900
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 561EE146
P 2900 2000
F 0 "R6" V 2900 2000 50  0000 C CNN
F 1 "47" V 2850 2150 50  0000 C CNN
F 2 "" V 2830 2000 30  0000 C CNN
F 3 "" H 2900 2000 30  0000 C CNN
	1    2900 2000
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 561EE18B
P 2900 2100
F 0 "R7" V 2900 2100 50  0000 C CNN
F 1 "47" V 2850 2250 50  0000 C CNN
F 2 "" V 2830 2100 30  0000 C CNN
F 3 "" H 2900 2100 30  0000 C CNN
	1    2900 2100
	0    1    1    0   
$EndComp
$Comp
L R R8
U 1 1 561EE1D4
P 2900 2200
F 0 "R8" V 2900 2200 50  0000 C CNN
F 1 "47" V 2850 2350 50  0000 C CNN
F 2 "" V 2830 2200 30  0000 C CNN
F 3 "" H 2900 2200 30  0000 C CNN
	1    2900 2200
	0    1    1    0   
$EndComp
Text Label 4500 1500 0    60   ~ 0
D0.A
Text Label 4500 1600 0    60   ~ 0
D1.A
Text Label 4500 1700 0    60   ~ 0
D2.A
Text Label 4500 1800 0    60   ~ 0
D3.A
Text Label 4500 1900 0    60   ~ 0
D4.A
Text Label 4500 2000 0    60   ~ 0
D5.A
Text Label 4500 2100 0    60   ~ 0
D6.A
Text Label 4500 2200 0    60   ~ 0
D7.A
Text Label 4500 4100 0    60   ~ 0
A6.A
Text Label 4500 2800 0    60   ~ 0
DBIN.A
Text Label 4500 2900 0    60   ~ 0
A2.A
Text Label 4500 3000 0    60   ~ 0
A1.A
Text Label 4500 3100 0    60   ~ 0
A0.A
Text Label 4500 3200 0    60   ~ 0
~MEMEN.A
Text Label 4500 3300 0    60   ~ 0
A3.A
Text Label 4500 3400 0    60   ~ 0
A4.A
Text Label 4500 3500 0    60   ~ 0
A5.A
Text Label 4500 4200 0    60   ~ 0
A7.A
Text Label 4500 4300 0    60   ~ 0
A8.A
Text Label 4500 4400 0    60   ~ 0
A9.A
Text Label 4500 4500 0    60   ~ 0
A10.A
Text Label 4500 4600 0    60   ~ 0
A11.A
Text Label 4500 4700 0    60   ~ 0
A12.A
Text Label 4500 4800 0    60   ~ 0
A13.A
Text Label 4500 5400 0    60   ~ 0
A14.A
Text Label 4500 5500 0    60   ~ 0
A15/CRUOUT.A
Text Label 4500 5600 0    60   ~ 0
~WE.A
Text Label 4500 5700 0    60   ~ 0
~CRULCK.A
Text Label 4500 5800 0    60   ~ 0
~RESET.A
Text Label 4500 5900 0    60   ~ 0
~Ø3.A
Text Label 4900 6650 2    60   ~ 0
~RDBENA.A
Entry Wire Line
	4950 6650 5050 6550
$Comp
L LED CR1
U 1 1 561F143F
P 5500 1950
F 0 "CR1" H 5500 2050 50  0000 C CNN
F 1 "LED" H 5500 1850 50  0000 C CNN
F 2 "" H 5500 1950 60  0000 C CNN
F 3 "" H 5500 1950 60  0000 C CNN
	1    5500 1950
	0    -1   -1   0   
$EndComp
$Comp
L S8050 Q2
U 1 1 561F161F
P 5400 2400
F 0 "Q2" H 5600 2475 50  0000 L CNN
F 1 "2N3904" H 5600 2400 50  0000 L CNN
F 2 "TO-92" H 5600 2325 50  0000 L CIN
F 3 "" H 5400 2400 50  0000 L CNN
	1    5400 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 561F1CB5
P 5500 3000
F 0 "#PWR?" H 5500 2750 50  0001 C CNN
F 1 "GND" H 5500 2850 50  0000 C CNN
F 2 "" H 5500 3000 60  0000 C CNN
F 3 "" H 5500 3000 60  0000 C CNN
	1    5500 3000
	1    0    0    -1  
$EndComp
$Comp
L R R18
U 1 1 561F1D23
P 5500 2800
F 0 "R18" V 5580 2800 50  0000 C CNN
F 1 "100" V 5500 2800 50  0000 C CNN
F 2 "" V 5430 2800 30  0000 C CNN
F 3 "" H 5500 2800 30  0000 C CNN
	1    5500 2800
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 561F283F
P 7500 1700
F 0 "#PWR?" H 7500 1550 50  0001 C CNN
F 1 "VCC" H 7500 1850 50  0000 C CNN
F 2 "" H 7500 1700 60  0000 C CNN
F 3 "" H 7500 1700 60  0000 C CNN
	1    7500 1700
	1    0    0    -1  
$EndComp
Entry Wire Line
	2450 6950 2550 6850
Entry Wire Line
	2450 7050 2550 6950
Entry Wire Line
	4950 6850 5050 6750
Entry Wire Line
	4950 6950 5050 6850
Entry Wire Line
	2450 7150 2550 7050
Entry Wire Line
	4950 7050 5050 6950
Entry Wire Line
	2450 7250 2550 7150
Entry Wire Line
	4950 7150 5050 7050
$Comp
L R R19
U 1 1 561F3986
P 2650 6650
F 0 "R19" V 2730 6650 50  0000 C CNN
F 1 "10k" V 2650 6650 50  0000 C CNN
F 2 "" V 2580 6650 30  0000 C CNN
F 3 "" H 2650 6650 30  0000 C CNN
	1    2650 6650
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 561F3AE2
P 2650 6500
F 0 "#PWR?" H 2650 6350 50  0001 C CNN
F 1 "VCC" H 2650 6650 50  0000 C CNN
F 2 "" H 2650 6500 60  0000 C CNN
F 3 "" H 2650 6500 60  0000 C CNN
	1    2650 6500
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR?
U 1 1 561F3B79
P 4350 6300
F 0 "#PWR?" H 4350 6150 50  0001 C CNN
F 1 "VCC" H 4350 6450 50  0000 C CNN
F 2 "" H 4350 6300 60  0000 C CNN
F 3 "" H 4350 6300 60  0000 C CNN
	1    4350 6300
	1    0    0    -1  
$EndComp
Text Label 3600 6850 2    60   ~ 0
READY
Text Label 3600 6950 2    60   ~ 0
CRUIN
Text Label 3600 7050 2    60   ~ 0
~EXTINT
Text Label 3600 7150 2    60   ~ 0
AUDIOIN
Wire Bus Line
	10500 1300 10500 5000
Wire Bus Line
	9200 1300 9200 5000
Wire Wire Line
	8650 2050 10350 2050
Wire Wire Line
	9550 2200 9600 2200
Wire Wire Line
	9100 2300 9600 2300
Wire Wire Line
	9350 2400 9600 2400
Wire Wire Line
	9350 2500 9600 2500
Wire Wire Line
	9300 2700 9600 2700
Wire Wire Line
	9300 2800 9600 2800
Wire Wire Line
	9300 2900 9600 2900
Wire Wire Line
	9300 3000 9600 3000
Wire Wire Line
	9300 3100 9600 3100
Wire Wire Line
	9300 3200 9600 3200
Wire Wire Line
	9300 3300 9600 3300
Wire Wire Line
	9300 3400 9600 3400
Wire Wire Line
	9350 3500 9600 3500
Wire Wire Line
	9300 3600 9600 3600
Wire Wire Line
	9300 3700 9600 3700
Wire Wire Line
	9300 3800 9600 3800
Wire Wire Line
	9300 3900 9600 3900
Wire Wire Line
	9300 4000 9600 4000
Wire Wire Line
	9300 4100 9600 4100
Wire Wire Line
	9300 4200 9600 4200
Wire Wire Line
	9300 4300 9600 4300
Wire Wire Line
	9150 4400 9600 4400
Wire Wire Line
	9350 4500 9600 4500
Wire Wire Line
	9350 4600 9600 4600
Wire Wire Line
	9300 4700 9600 4700
Wire Wire Line
	9350 4800 9600 4800
Wire Wire Line
	9300 4900 9600 4900
Wire Wire Line
	9500 5000 9600 5000
Wire Wire Line
	9550 5100 9600 5100
Wire Wire Line
	10100 2200 10400 2200
Wire Wire Line
	10100 2300 10400 2300
Wire Wire Line
	10100 2400 10400 2400
Wire Wire Line
	10100 2600 10400 2600
Wire Wire Line
	10100 2700 10550 2700
Wire Wire Line
	10100 2900 10550 2900
Wire Wire Line
	10100 3100 10350 3100
Wire Wire Line
	10100 3200 10400 3200
Wire Wire Line
	10100 3300 10400 3300
Wire Wire Line
	10100 3400 10400 3400
Wire Wire Line
	10100 3500 10400 3500
Wire Wire Line
	10100 3600 10400 3600
Wire Wire Line
	10100 3700 10400 3700
Wire Wire Line
	10100 3800 10400 3800
Wire Wire Line
	10100 3900 10400 3900
Wire Wire Line
	10100 4000 10400 4000
Wire Wire Line
	10100 4100 10400 4100
Wire Wire Line
	10100 4200 10400 4200
Wire Wire Line
	10100 4300 10400 4300
Wire Wire Line
	10400 4500 10100 4500
Wire Wire Line
	10100 4600 10400 4600
Wire Wire Line
	10100 4700 10400 4700
Wire Wire Line
	10100 4800 10400 4800
Wire Wire Line
	10100 4900 10400 4900
Wire Wire Line
	10100 5000 10400 5000
Wire Wire Line
	10100 5100 10400 5100
Wire Wire Line
	9550 2050 9550 2200
Wire Wire Line
	10350 2050 10350 2200
Connection ~ 10350 2200
Wire Wire Line
	9350 2300 9350 4800
Connection ~ 9350 2400
Connection ~ 9350 2500
Connection ~ 9350 3500
Connection ~ 9350 4500
Connection ~ 9350 4600
Wire Wire Line
	9100 2350 9100 2300
Connection ~ 9350 2300
Wire Wire Line
	9550 5100 9550 5200
Wire Wire Line
	9550 5200 10300 5200
Wire Wire Line
	10300 5200 10300 5100
Connection ~ 10300 5100
Wire Wire Line
	9500 5000 9500 5250
Wire Wire Line
	9500 5250 10350 5250
Wire Wire Line
	10350 5250 10350 5000
Connection ~ 10350 5000
Wire Wire Line
	8800 2750 8800 5300
Wire Wire Line
	10100 4400 10350 4400
Wire Wire Line
	8800 5300 10900 5300
Wire Wire Line
	10900 5300 10900 2700
Wire Wire Line
	7500 2750 8850 2750
Wire Wire Line
	8850 2850 8800 2850
Connection ~ 8800 2850
Wire Wire Line
	8850 4400 8800 4400
Connection ~ 8800 4400
Wire Wire Line
	10850 4450 10900 4450
Connection ~ 10900 4450
Wire Wire Line
	10850 4350 10900 4350
Connection ~ 10900 4350
Wire Wire Line
	10900 2700 10850 2700
Wire Wire Line
	10850 2900 10900 2900
Connection ~ 10900 2900
Wire Wire Line
	10350 4400 10350 4350
Wire Wire Line
	10350 4350 10550 4350
Wire Wire Line
	10400 4500 10400 4450
Wire Wire Line
	10400 4450 10550 4450
Wire Wire Line
	9300 2900 9300 2850
Wire Wire Line
	9300 2850 9150 2850
Wire Wire Line
	9300 2800 9300 2750
Wire Wire Line
	9300 2750 9150 2750
Wire Wire Line
	7500 2050 7850 2050
Wire Wire Line
	7500 1700 7500 2750
Connection ~ 8800 2750
Wire Wire Line
	8250 2350 8250 2450
Wire Wire Line
	8850 2100 8850 2050
Connection ~ 8850 2050
Wire Wire Line
	8700 2100 8700 2050
Connection ~ 8700 2050
Wire Wire Line
	7800 2100 7800 2050
Connection ~ 7800 2050
Wire Wire Line
	7650 2100 7650 2050
Connection ~ 7650 2050
Connection ~ 9550 2050
Connection ~ 8250 2400
Wire Wire Line
	7650 2400 8850 2400
Wire Wire Line
	8850 2400 8850 2300
Wire Wire Line
	8700 2300 8700 2400
Connection ~ 8700 2400
Wire Wire Line
	7650 2300 7650 2400
Wire Wire Line
	7800 2300 7800 2400
Connection ~ 7800 2400
Wire Wire Line
	10650 3150 10650 3050
Wire Wire Line
	10350 3100 10350 3050
Wire Wire Line
	10350 3050 10650 3050
Wire Wire Line
	1400 2800 2350 2800
Wire Wire Line
	1400 2900 2350 2900
Wire Wire Line
	1400 3000 2300 3000
Wire Wire Line
	1400 3100 2350 3100
Wire Wire Line
	1400 3200 2350 3200
Wire Wire Line
	1400 3300 2350 3300
Wire Wire Line
	1400 3400 2300 3400
Wire Wire Line
	1400 3500 2350 3500
Wire Wire Line
	2300 3600 1400 3600
Wire Wire Line
	1400 3700 2350 3700
Wire Wire Line
	1400 3800 2350 3800
Wire Wire Line
	1400 3900 2350 3900
Wire Wire Line
	1400 4000 2350 4000
Wire Wire Line
	1400 4100 2350 4100
Wire Wire Line
	1400 4200 2350 4200
Wire Wire Line
	1400 4300 2350 4300
Wire Wire Line
	1400 4400 2350 4400
Wire Wire Line
	1400 4500 2350 4500
Wire Wire Line
	1400 4600 2350 4600
Wire Wire Line
	1400 4700 2350 4700
Wire Wire Line
	1400 4800 2350 4800
Wire Wire Line
	1400 4900 2350 4900
Wire Wire Line
	1400 5000 2350 5000
Wire Wire Line
	1400 5100 2350 5100
Wire Wire Line
	2300 5200 1400 5200
Wire Wire Line
	1400 5300 2350 5300
Wire Wire Line
	2300 5400 1400 5400
Wire Wire Line
	1400 5500 2350 5500
Wire Wire Line
	1400 5600 2350 5600
Wire Wire Line
	1400 5700 2350 5700
Wire Wire Line
	1400 5800 2350 5800
Wire Wire Line
	2300 5900 1400 5900
Wire Wire Line
	1400 6000 2350 6000
Wire Wire Line
	1400 6100 2350 6100
Wire Wire Line
	1400 6200 2350 6200
Wire Wire Line
	1400 6300 2350 6300
Wire Wire Line
	2300 6400 1400 6400
Wire Wire Line
	1400 6500 2350 6500
Wire Wire Line
	1400 6600 2350 6600
Wire Wire Line
	1400 6700 2350 6700
Wire Wire Line
	1400 6800 2350 6800
Wire Wire Line
	1400 6900 2350 6900
Wire Wire Line
	1400 7000 2350 7000
Wire Wire Line
	1400 7100 2300 7100
Connection ~ 1800 2800
Connection ~ 1800 2900
Wire Wire Line
	2300 3000 2300 6400
Connection ~ 1800 3400
Connection ~ 1800 3000
Connection ~ 1800 3600
Connection ~ 2300 3400
Connection ~ 1800 3100
Connection ~ 1800 3200
Connection ~ 1800 3300
Connection ~ 1800 3500
Connection ~ 1800 3700
Connection ~ 1800 5200
Connection ~ 2300 3600
Connection ~ 1800 5400
Connection ~ 2300 5200
Connection ~ 1800 5900
Connection ~ 2300 5400
Connection ~ 1800 6400
Connection ~ 2300 5900
Connection ~ 1800 6900
Wire Wire Line
	2300 7100 2300 6900
Connection ~ 1800 7100
Connection ~ 1800 7000
Connection ~ 1800 3800
Connection ~ 1800 3900
Connection ~ 1800 4000
Connection ~ 1800 4100
Connection ~ 1800 4200
Connection ~ 1800 4300
Connection ~ 1800 4400
Connection ~ 1800 4500
Connection ~ 1800 4600
Connection ~ 1800 4700
Connection ~ 1800 4800
Connection ~ 1800 4900
Connection ~ 1800 5000
Connection ~ 1800 5100
Connection ~ 1800 5300
Connection ~ 1800 5500
Connection ~ 1800 5600
Connection ~ 1800 5700
Connection ~ 1800 5800
Connection ~ 1800 6000
Connection ~ 1800 6100
Connection ~ 1800 6200
Connection ~ 1800 6300
Connection ~ 1800 6500
Connection ~ 1800 6600
Connection ~ 1800 6700
Connection ~ 1800 6800
Wire Bus Line
	2450 1600 2450 7350
Wire Bus Line
	5050 1300 10500 1300
Wire Wire Line
	2550 2800 3100 2800
Wire Wire Line
	2550 2900 3100 2900
Wire Wire Line
	2550 3000 3100 3000
Wire Wire Line
	2550 3100 3100 3100
Wire Wire Line
	2550 3200 3100 3200
Wire Wire Line
	2550 3300 3100 3300
Wire Wire Line
	2550 3400 3100 3400
Wire Wire Line
	2550 3500 3100 3500
Wire Wire Line
	2550 4100 3100 4100
Wire Wire Line
	2550 4200 3100 4200
Wire Wire Line
	2550 4300 3100 4300
Wire Wire Line
	2550 4400 3100 4400
Wire Wire Line
	2550 4500 3100 4500
Wire Wire Line
	2550 4600 3100 4600
Wire Wire Line
	2550 4700 3100 4700
Wire Wire Line
	2550 4800 3100 4800
Wire Wire Line
	2550 5400 3100 5400
Wire Wire Line
	2550 5500 3100 5500
Wire Wire Line
	2550 5600 3100 5600
Wire Wire Line
	2550 5700 3100 5700
Wire Wire Line
	2550 5800 3100 5800
Wire Wire Line
	2550 5900 3100 5900
Wire Wire Line
	2950 6000 3100 6000
Wire Wire Line
	2950 6100 3100 6100
Wire Wire Line
	3100 3700 3050 3700
Wire Wire Line
	3050 3700 3050 3850
Wire Wire Line
	3050 3800 3100 3800
Wire Wire Line
	3100 5000 3050 5000
Wire Wire Line
	3050 5000 3050 5150
Wire Wire Line
	3050 5100 3100 5100
Wire Wire Line
	3100 6300 3050 6300
Wire Wire Line
	3050 6300 3050 6450
Wire Wire Line
	3050 6400 3100 6400
Connection ~ 3050 3800
Connection ~ 3050 5100
Connection ~ 3050 6400
Wire Wire Line
	2950 6000 2950 6650
Wire Wire Line
	2950 6650 4950 6650
Connection ~ 2950 6100
Wire Wire Line
	4500 6100 4550 6100
Wire Wire Line
	4550 6100 4550 6750
Wire Wire Line
	4550 6750 3950 6750
Wire Wire Line
	3650 6750 2550 6750
Wire Wire Line
	4350 6600 4350 6650
Connection ~ 4350 6650
Wire Wire Line
	4500 6000 4850 6000
Wire Wire Line
	4850 6000 4850 2500
Wire Wire Line
	4850 2500 4500 2500
Wire Wire Line
	4500 2400 5200 2400
Wire Wire Line
	4900 2400 4900 2800
Wire Wire Line
	4500 2800 4950 2800
Wire Wire Line
	3050 1500 3100 1500
Wire Wire Line
	3050 1600 3100 1600
Wire Wire Line
	3050 1700 3100 1700
Wire Wire Line
	3050 1800 3100 1800
Wire Wire Line
	3050 1900 3100 1900
Wire Wire Line
	3050 2000 3100 2000
Wire Wire Line
	3050 2100 3100 2100
Wire Wire Line
	3050 2200 3100 2200
Wire Bus Line
	5050 1300 5050 7150
Wire Wire Line
	4500 1500 4950 1500
Wire Wire Line
	4500 1600 4950 1600
Wire Wire Line
	4500 1700 4950 1700
Wire Wire Line
	4500 1800 4950 1800
Wire Wire Line
	4500 1900 4950 1900
Wire Wire Line
	4500 2000 4950 2000
Wire Wire Line
	4500 2100 4950 2100
Wire Wire Line
	4500 2200 4950 2200
Connection ~ 4900 2800
Wire Wire Line
	4500 2900 4950 2900
Wire Wire Line
	4500 3000 4950 3000
Wire Wire Line
	4500 3100 4950 3100
Wire Wire Line
	4500 3200 4950 3200
Wire Wire Line
	4500 3300 4950 3300
Wire Wire Line
	4500 3400 4950 3400
Wire Wire Line
	4500 3500 4950 3500
Wire Wire Line
	4500 4100 4950 4100
Wire Wire Line
	4500 4200 4950 4200
Wire Wire Line
	4500 4300 4950 4300
Wire Wire Line
	4500 4400 4950 4400
Wire Wire Line
	4500 4500 4950 4500
Wire Wire Line
	4500 4600 4950 4600
Wire Wire Line
	4500 4700 4950 4700
Wire Wire Line
	4500 4800 4950 4800
Wire Wire Line
	4500 5400 4950 5400
Wire Wire Line
	4500 5500 4950 5500
Wire Wire Line
	4500 5600 4950 5600
Wire Wire Line
	4500 5700 4950 5700
Wire Wire Line
	4500 5800 4950 5800
Wire Wire Line
	4500 5900 4950 5900
Wire Wire Line
	2550 1500 2750 1500
Wire Wire Line
	2550 1600 2750 1600
Wire Wire Line
	2550 1700 2750 1700
Wire Wire Line
	2550 1800 2750 1800
Wire Wire Line
	2550 1900 2750 1900
Wire Wire Line
	2550 2000 2750 2000
Wire Wire Line
	2550 2100 2750 2100
Wire Wire Line
	2550 2200 2750 2200
Wire Wire Line
	5500 1700 5500 1750
Wire Wire Line
	5500 2150 5500 2200
Connection ~ 4900 2400
Wire Wire Line
	5500 2600 5500 2650
Wire Wire Line
	5500 2950 5500 3000
Wire Wire Line
	5500 1700 7500 1700
Connection ~ 7500 2050
Wire Wire Line
	2550 6850 4950 6850
Wire Wire Line
	2550 6950 4950 6950
Wire Wire Line
	2550 7050 4950 7050
Wire Wire Line
	2550 7150 4950 7150
Wire Wire Line
	2550 7250 4950 7250
Wire Wire Line
	2650 6800 2650 6850
Connection ~ 2650 6850
Text Label 3600 7250 2    60   ~ 0
Unreg.+8V
Entry Wire Line
	4950 7250 5050 7150
Entry Wire Line
	2450 7350 2550 7250
Connection ~ 2300 6900
Entry Wire Line
	2350 6900 2450 6800
Text Label 1850 6900 0    60   ~ 0
Unreg.+8V
$Comp
L C_Small C4
U 1 1 561F5511
P 3700 2650
F 0 "C4" H 3710 2720 50  0000 L CNN
F 1 "1" H 3710 2570 50  0000 L CNN
F 2 "" H 3700 2650 60  0000 C CNN
F 3 "" H 3700 2650 60  0000 C CNN
	1    3700 2650
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C5
U 1 1 561F5680
P 3700 3950
F 0 "C5" H 3710 4020 50  0000 L CNN
F 1 "1" H 3710 3870 50  0000 L CNN
F 2 "" H 3700 3950 60  0000 C CNN
F 3 "" H 3700 3950 60  0000 C CNN
	1    3700 3950
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C6
U 1 1 561F56DA
P 3700 5250
F 0 "C6" H 3710 5320 50  0000 L CNN
F 1 "1" H 3710 5170 50  0000 L CNN
F 2 "" H 3700 5250 60  0000 C CNN
F 3 "" H 3700 5250 60  0000 C CNN
	1    3700 5250
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C7
U 1 1 561F5732
P 4000 1350
F 0 "C7" H 4010 1420 50  0000 L CNN
F 1 "1" H 4010 1270 50  0000 L CNN
F 2 "" H 4000 1350 60  0000 C CNN
F 3 "" H 4000 1350 60  0000 C CNN
	1    4000 1350
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 561F5B05
P 4100 1350
F 0 "#PWR?" H 4100 1100 50  0001 C CNN
F 1 "GND" H 4100 1200 50  0000 C CNN
F 2 "" H 4100 1350 60  0000 C CNN
F 3 "" H 4100 1350 60  0000 C CNN
	1    4100 1350
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 561F5B5F
P 3800 5250
F 0 "#PWR?" H 3800 5000 50  0001 C CNN
F 1 "GND" H 3800 5100 50  0000 C CNN
F 2 "" H 3800 5250 60  0000 C CNN
F 3 "" H 3800 5250 60  0000 C CNN
	1    3800 5250
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 561F5BB7
P 3800 3950
F 0 "#PWR?" H 3800 3700 50  0001 C CNN
F 1 "GND" H 3800 3800 50  0000 C CNN
F 2 "" H 3800 3950 60  0000 C CNN
F 3 "" H 3800 3950 60  0000 C CNN
	1    3800 3950
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 561F5C0F
P 3800 2650
F 0 "#PWR?" H 3800 2400 50  0001 C CNN
F 1 "GND" H 3800 2500 50  0000 C CNN
F 2 "" H 3800 2650 60  0000 C CNN
F 3 "" H 3800 2650 60  0000 C CNN
	1    3800 2650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3900 1350 3800 1350
Wire Wire Line
	3800 1350 3800 1450
Wire Wire Line
	3600 2650 3500 2650
Wire Wire Line
	3500 2650 3500 2750
Wire Wire Line
	3600 3950 3500 3950
Wire Wire Line
	3500 3950 3500 4050
Wire Wire Line
	3600 5250 3500 5250
Wire Wire Line
	3500 5250 3500 5350
$EndSCHEMATC
