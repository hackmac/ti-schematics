EESchema Schematic File Version 2
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 10
Title "TI-99/4A"
Date "2015-09-28"
Rev "1"
Comp "HackMac"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCM6810P U608
U 1 1 561CF12E
P 3450 1850
F 0 "U608" H 3750 2700 60  0000 R CNN
F 1 "MCM6810P" V 3450 1850 60  0000 C CNN
F 2 "Sockets_DIP:DIP-24__600" H 3450 1850 60  0001 C CNN
F 3 "" H 3450 1850 60  0000 C CNN
	1    3450 1850
	0    -1   -1   0   
$EndComp
$Comp
L MCM6810P U609
U 1 1 561CF176
P 3450 3350
F 0 "U609" H 3750 4200 60  0000 R CNN
F 1 "MCM6810P" V 3450 3350 60  0000 C CNN
F 2 "Sockets_DIP:DIP-24__600" H 3450 3350 60  0001 C CNN
F 3 "" H 3450 3350 60  0000 C CNN
	1    3450 3350
	0    -1   1    0   
$EndComp
Entry Wire Line
	3350 1100 3450 1200
Entry Wire Line
	3250 1100 3350 1200
Entry Wire Line
	3150 1100 3250 1200
Entry Wire Line
	3050 1100 3150 1200
Entry Wire Line
	2950 1100 3050 1200
Entry Wire Line
	2850 1100 2950 1200
Entry Wire Line
	2750 1100 2850 1200
Entry Wire Line
	2650 1100 2750 1200
Entry Wire Line
	3250 4100 3350 4000
Entry Wire Line
	3350 4100 3450 4000
Entry Wire Line
	3150 4100 3250 4000
Entry Wire Line
	3050 4100 3150 4000
Entry Wire Line
	2950 4100 3050 4000
Entry Wire Line
	2850 4100 2950 4000
Entry Wire Line
	2750 4100 2850 4000
Entry Wire Line
	2650 4100 2750 4000
Entry Wire Line
	3250 2600 3350 2500
Entry Wire Line
	3150 2600 3250 2500
Entry Wire Line
	3050 2600 3150 2500
Entry Wire Line
	2950 2600 3050 2500
Entry Wire Line
	2850 2600 2950 2500
Entry Wire Line
	2750 2600 2850 2500
Entry Wire Line
	2650 2600 2750 2500
Entry Wire Line
	2650 2600 2750 2700
Entry Wire Line
	2750 2600 2850 2700
Entry Wire Line
	2850 2600 2950 2700
Entry Wire Line
	2950 2600 3050 2700
Entry Wire Line
	3050 2600 3150 2700
Entry Wire Line
	3150 2600 3250 2700
Entry Wire Line
	3250 2600 3350 2700
Text Label 3450 3850 3    60   ~ 0
D15
Text Label 3350 3850 3    60   ~ 0
D14
Text Label 3250 3850 3    60   ~ 0
D13
Text Label 3150 3850 3    60   ~ 0
D12
Text Label 3050 3850 3    60   ~ 0
D11
Text Label 2950 3850 3    60   ~ 0
D10
Text Label 2850 3850 3    60   ~ 0
D9
Text Label 2750 3850 3    60   ~ 0
D8
Text Label 3450 1350 1    60   ~ 0
D7
Text Label 3350 1350 1    60   ~ 0
D6
Text Label 3250 1350 1    60   ~ 0
D5
Text Label 3150 1350 1    60   ~ 0
D4
Text Label 3050 1350 1    60   ~ 0
D3
Text Label 2950 1350 1    60   ~ 0
D2
Text Label 2850 1350 1    60   ~ 0
D1
Text Label 2750 1350 1    60   ~ 0
D0
Text Label 3350 2850 1    60   ~ 0
A14
Text Label 3250 2850 1    60   ~ 0
A13
Text Label 3150 2850 1    60   ~ 0
A12
Text Label 3050 2850 1    60   ~ 0
A11
Text Label 2950 2850 1    60   ~ 0
A10
Text Label 2850 2850 1    60   ~ 0
A9
Text Label 2750 2850 1    60   ~ 0
A8
Text Label 3350 2350 3    60   ~ 0
A14
Text Label 3250 2350 3    60   ~ 0
A13
Text Label 3150 2350 3    60   ~ 0
A12
Text Label 3050 2350 3    60   ~ 0
A11
Text Label 2950 2350 3    60   ~ 0
A10
Text Label 2850 2350 3    60   ~ 0
A9
Text Label 2750 2350 3    60   ~ 0
A8
$Comp
L GND #PWR067
U 1 1 561D045F
P 4150 2800
F 0 "#PWR067" H 4150 2550 50  0001 C CNN
F 1 "GND" H 4150 2650 50  0000 C CNN
F 2 "" H 4150 2800 60  0000 C CNN
F 3 "" H 4150 2800 60  0000 C CNN
	1    4150 2800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR068
U 1 1 561D0479
P 4150 2400
F 0 "#PWR068" H 4150 2250 50  0001 C CNN
F 1 "+5V" H 4150 2540 50  0000 C CNN
F 2 "" H 4150 2400 60  0000 C CNN
F 3 "" H 4150 2400 60  0000 C CNN
	1    4150 2400
	1    0    0    -1  
$EndComp
Entry Wire Line
	1400 1300 1500 1400
Entry Wire Line
	1400 1400 1500 1500
Entry Wire Line
	1400 1500 1500 1600
Entry Wire Line
	1400 1600 1500 1700
Entry Wire Line
	1400 1700 1500 1800
Entry Wire Line
	1400 1800 1500 1900
Entry Wire Line
	1400 1900 1500 2000
Entry Wire Line
	1400 2000 1500 2100
Entry Wire Line
	1400 2100 1500 2200
Entry Wire Line
	1400 2200 1500 2300
Entry Wire Line
	1400 2300 1500 2400
Entry Wire Line
	1400 2400 1500 2500
Entry Wire Line
	1400 2500 1500 2600
Entry Wire Line
	1400 2600 1500 2700
Entry Wire Line
	1400 2700 1500 2800
Entry Wire Line
	1400 2800 1500 2900
Text HLabel 1200 1300 0    60   BiDi ~ 0
U600-41
Text HLabel 1200 1400 0    60   BiDi ~ 0
U600-42
Text HLabel 1200 1500 0    60   BiDi ~ 0
U600-43
Text HLabel 1200 1600 0    60   BiDi ~ 0
U600-44
Text HLabel 1200 1700 0    60   BiDi ~ 0
U600-45
Text HLabel 1200 1800 0    60   BiDi ~ 0
U600-46
Text HLabel 1200 1900 0    60   BiDi ~ 0
U600-47
Text HLabel 1200 2000 0    60   BiDi ~ 0
U600-48
Text HLabel 1200 2100 0    60   BiDi ~ 0
U600-49
Text HLabel 1200 2200 0    60   BiDi ~ 0
U600-50
Text HLabel 1200 2300 0    60   BiDi ~ 0
U600-51
Text HLabel 1200 2400 0    60   BiDi ~ 0
U600-52
Text HLabel 1200 2500 0    60   BiDi ~ 0
U600-53
Text HLabel 1200 2600 0    60   BiDi ~ 0
U600-54
Text HLabel 1200 2700 0    60   BiDi ~ 0
U600-55
Text HLabel 1200 2800 0    60   BiDi ~ 0
U600-56
Text Label 1400 1300 2    60   ~ 0
D0
Text Label 1400 1400 2    60   ~ 0
D1
Text Label 1400 1500 2    60   ~ 0
D2
Text Label 1400 1600 2    60   ~ 0
D3
Text Label 1400 1700 2    60   ~ 0
D4
Text Label 1400 1800 2    60   ~ 0
D5
Text Label 1400 1900 2    60   ~ 0
D6
Text Label 1400 2000 2    60   ~ 0
D7
Text Label 1400 2100 2    60   ~ 0
D8
Text Label 1400 2200 2    60   ~ 0
D9
Text Label 1400 2300 2    60   ~ 0
D10
Text Label 1400 2400 2    60   ~ 0
D11
Text Label 1400 2500 2    60   ~ 0
D12
Text Label 1400 2600 2    60   ~ 0
D13
Text Label 1400 2700 2    60   ~ 0
D14
Text Label 1400 2800 2    60   ~ 0
D15
Text HLabel 2100 1300 0    60   Input ~ 0
U600-10
Entry Wire Line
	2350 1300 2450 1400
Entry Wire Line
	2350 1400 2450 1500
Entry Wire Line
	2350 1500 2450 1600
Entry Wire Line
	2350 1600 2450 1700
Entry Wire Line
	2350 1700 2450 1800
Entry Wire Line
	2350 1800 2450 1900
Entry Wire Line
	2350 1900 2450 2000
Entry Wire Line
	2350 2000 2450 2100
Entry Wire Line
	2350 2100 2450 2200
Entry Wire Line
	2350 2200 2450 2300
Entry Wire Line
	2350 2300 2450 2400
Entry Wire Line
	2350 2400 2450 2500
Text HLabel 2100 1400 0    60   Input ~ 0
U600-11
Text HLabel 2100 1500 0    60   Input ~ 0
U600-12
Text HLabel 2100 1600 0    60   Input ~ 0
U600-13
Text HLabel 2100 1700 0    60   Input ~ 0
U600-14
Text HLabel 2100 1800 0    60   Input ~ 0
U600-15
Text HLabel 2100 1900 0    60   Input ~ 0
U600-16
Text HLabel 2100 2000 0    60   Input ~ 0
U600-17
Text HLabel 2100 2100 0    60   Input ~ 0
U600-18
Text HLabel 2100 2200 0    60   Input ~ 0
U600-19
Text HLabel 2100 2300 0    60   Input ~ 0
U600-20
Text HLabel 2100 2400 0    60   Input ~ 0
U600-21
Text Label 2350 1300 2    60   ~ 0
A14
Text Label 2350 1400 2    60   ~ 0
A13
Text Label 2350 1500 2    60   ~ 0
A12
Text Label 2350 1600 2    60   ~ 0
A11
Text Label 2350 1700 2    60   ~ 0
A10
Text Label 2350 1800 2    60   ~ 0
A9
Text Label 2350 1900 2    60   ~ 0
A8
Text Label 2350 2000 2    60   ~ 0
A7
Text Label 2350 2100 2    60   ~ 0
A6
Text Label 2350 2200 2    60   ~ 0
A5
Text Label 2350 2300 2    60   ~ 0
A4
Text Label 2350 2400 2    60   ~ 0
A3
Text HLabel 1200 4450 0    60   Input ~ 0
U507-8
Text Label 4450 2700 2    60   ~ 0
~MB
Text HLabel 1200 4350 0    60   Input ~ 0
U600-61
Text Label 4450 2800 2    60   ~ 0
~WE
Text Label 1550 4350 2    60   ~ 0
~WE
Entry Wire Line
	4450 2700 4550 2800
Entry Wire Line
	4450 2800 4550 2900
Entry Wire Line
	1550 4350 1650 4250
Entry Wire Line
	1550 4450 1650 4350
Entry Wire Line
	1550 4550 1650 4450
Text Label 1550 4450 2    60   ~ 0
~MB
Text HLabel 1200 4550 0    60   Input ~ 0
R527-2
Text Label 1550 4550 2    60   ~ 0
~ROMEN
$Comp
L TMS4732 U610
U 1 1 5621AE29
P 5750 1850
F 0 "U610" H 6050 2550 60  0000 R CNN
F 1 "TMS4732" V 5750 1850 60  0000 C CNN
F 2 "Sockets_DIP:DIP-24__600" H 5750 1700 60  0001 C CNN
F 3 "" H 5750 1700 60  0000 C CNN
	1    5750 1850
	0    1    -1   0   
$EndComp
$Comp
L TMS4732 U611
U 1 1 5621AE79
P 5750 3350
F 0 "U611" H 6050 4050 60  0000 R CNN
F 1 "TMS4732" V 5750 3350 60  0000 C CNN
F 2 "Sockets_DIP:DIP-24__600" H 5750 3200 60  0001 C CNN
F 3 "" H 5750 3200 60  0000 C CNN
	1    5750 3350
	0    1    1    0   
$EndComp
Entry Wire Line
	6200 2600 6300 2700
Entry Wire Line
	6100 2600 6200 2700
Entry Wire Line
	6000 2600 6100 2700
Entry Wire Line
	5900 2600 6000 2700
Entry Wire Line
	5800 2600 5900 2700
Entry Wire Line
	5700 2600 5800 2700
Entry Wire Line
	5600 2600 5700 2700
Entry Wire Line
	5500 2600 5600 2700
Entry Wire Line
	5400 2600 5500 2700
Entry Wire Line
	5300 2600 5400 2700
Entry Wire Line
	5200 2600 5300 2700
Entry Wire Line
	5100 2600 5200 2700
Entry Wire Line
	6200 2600 6300 2500
Entry Wire Line
	6100 2600 6200 2500
Entry Wire Line
	6000 2600 6100 2500
Entry Wire Line
	5900 2600 6000 2500
Entry Wire Line
	5800 2600 5900 2500
Entry Wire Line
	5700 2600 5800 2500
Entry Wire Line
	5600 2600 5700 2500
Entry Wire Line
	5500 2600 5600 2500
Entry Wire Line
	5400 2600 5500 2500
Entry Wire Line
	5300 2600 5400 2500
Entry Wire Line
	5200 2600 5300 2500
Entry Wire Line
	5100 2600 5200 2500
Entry Wire Line
	6200 4100 6300 4000
Entry Wire Line
	6100 4100 6200 4000
Entry Wire Line
	6000 4100 6100 4000
Entry Wire Line
	5900 4100 6000 4000
Entry Wire Line
	5800 4100 5900 4000
Entry Wire Line
	5700 4100 5800 4000
Entry Wire Line
	5600 4100 5700 4000
Entry Wire Line
	5500 4100 5600 4000
Entry Wire Line
	6200 1100 6300 1200
Entry Wire Line
	6100 1100 6200 1200
Entry Wire Line
	6000 1100 6100 1200
Entry Wire Line
	5900 1100 6000 1200
Entry Wire Line
	5800 1100 5900 1200
Entry Wire Line
	5700 1100 5800 1200
Entry Wire Line
	5600 1100 5700 1200
Entry Wire Line
	5500 1100 5600 1200
Wire Bus Line
	1500 1100 6200 1100
Wire Bus Line
	1500 1100 1500 4100
Wire Bus Line
	1500 4100 6200 4100
Wire Wire Line
	3450 1200 3450 1350
Wire Wire Line
	3350 1200 3350 1350
Wire Wire Line
	3250 1200 3250 1350
Wire Wire Line
	3150 1200 3150 1350
Wire Wire Line
	3050 1200 3050 1350
Wire Wire Line
	2950 1200 2950 1350
Wire Wire Line
	2850 1350 2850 1200
Wire Wire Line
	2750 1200 2750 1350
Wire Wire Line
	2750 3850 2750 4000
Wire Wire Line
	2850 3850 2850 4000
Wire Wire Line
	2950 3850 2950 4000
Wire Wire Line
	3050 3850 3050 4000
Wire Wire Line
	3150 3850 3150 4000
Wire Wire Line
	3250 3850 3250 4000
Wire Wire Line
	3350 3850 3350 4000
Wire Wire Line
	3450 3850 3450 4000
Wire Bus Line
	2450 2600 6200 2600
Wire Bus Line
	2450 1400 2450 2600
Wire Wire Line
	2750 2350 2750 2500
Wire Wire Line
	2850 2350 2850 2500
Wire Wire Line
	2950 2350 2950 2500
Wire Wire Line
	3050 2350 3050 2500
Wire Wire Line
	3150 2350 3150 2500
Wire Wire Line
	3250 2350 3250 2500
Wire Wire Line
	3350 2350 3350 2500
Wire Wire Line
	3350 2700 3350 2850
Wire Wire Line
	3250 2700 3250 2850
Wire Wire Line
	3150 2700 3150 2850
Wire Wire Line
	3050 2700 3050 2850
Wire Wire Line
	2950 2700 2950 2850
Wire Wire Line
	2850 2700 2850 2850
Wire Wire Line
	2750 2700 2750 2850
Wire Wire Line
	4000 2350 4000 2850
Wire Wire Line
	3900 2350 3900 2850
Wire Wire Line
	3700 2350 3700 2850
Wire Wire Line
	3800 2350 3800 2850
Wire Wire Line
	3500 2350 3500 2850
Wire Wire Line
	3700 2800 4150 2800
Connection ~ 4000 2800
Connection ~ 3900 2800
Connection ~ 3700 2800
Wire Wire Line
	3500 2400 4150 2400
Connection ~ 3800 2400
Connection ~ 3500 2400
Wire Wire Line
	3600 2350 3600 2850
Wire Wire Line
	3600 2700 4450 2700
Connection ~ 3600 2700
Wire Wire Line
	1200 1300 1400 1300
Wire Wire Line
	1200 1400 1400 1400
Wire Wire Line
	1200 1500 1400 1500
Wire Wire Line
	1200 1600 1400 1600
Wire Wire Line
	1200 1700 1400 1700
Wire Wire Line
	1200 1800 1400 1800
Wire Wire Line
	1200 1900 1400 1900
Wire Wire Line
	1200 2000 1400 2000
Wire Wire Line
	1200 2100 1400 2100
Wire Wire Line
	1200 2200 1400 2200
Wire Wire Line
	1200 2300 1400 2300
Wire Wire Line
	1200 2400 1400 2400
Wire Wire Line
	1200 2500 1400 2500
Wire Wire Line
	1200 2600 1400 2600
Wire Wire Line
	1200 2700 1400 2700
Wire Wire Line
	1200 2800 1400 2800
Wire Wire Line
	2100 1300 2350 1300
Wire Wire Line
	2100 1400 2350 1400
Wire Wire Line
	2100 1500 2350 1500
Wire Wire Line
	2100 1600 2350 1600
Wire Wire Line
	2100 1700 2350 1700
Wire Wire Line
	2100 1800 2350 1800
Wire Wire Line
	2100 1900 2350 1900
Wire Wire Line
	2100 2000 2350 2000
Wire Wire Line
	2100 2100 2350 2100
Wire Wire Line
	2100 2200 2350 2200
Wire Wire Line
	2100 2300 2350 2300
Wire Wire Line
	2100 2400 2350 2400
Wire Wire Line
	3750 1350 3750 1300
Wire Wire Line
	3750 1300 4250 1300
Wire Wire Line
	4250 1300 4250 3900
Wire Wire Line
	4250 3900 3750 3900
Wire Wire Line
	3750 3900 3750 3850
Connection ~ 4250 2800
Wire Bus Line
	1650 4150 1650 4450
Wire Bus Line
	4550 4150 1650 4150
Wire Bus Line
	4550 2800 4550 4150
Wire Wire Line
	1200 4350 1550 4350
Wire Wire Line
	1200 4450 1550 4450
Wire Wire Line
	1200 4550 1550 4550
Wire Wire Line
	5200 2350 5200 2500
Wire Wire Line
	5300 2350 5300 2500
Wire Wire Line
	5400 2350 5400 2500
Wire Wire Line
	5500 2350 5500 2500
Wire Wire Line
	5600 2350 5600 2500
Wire Wire Line
	5700 2350 5700 2500
Wire Wire Line
	5800 2350 5800 2500
Wire Wire Line
	5900 2350 5900 2500
Wire Wire Line
	6000 2350 6000 2500
Wire Wire Line
	6100 2350 6100 2500
Wire Wire Line
	6200 2350 6200 2500
Wire Wire Line
	6300 2350 6300 2500
Wire Wire Line
	6300 2700 6300 2850
Wire Wire Line
	6200 2700 6200 2850
Wire Wire Line
	6100 2700 6100 2850
Wire Wire Line
	6000 2700 6000 2850
Wire Wire Line
	5900 2700 5900 2850
Wire Wire Line
	5800 2700 5800 2850
Wire Wire Line
	5700 2700 5700 2850
Wire Wire Line
	5600 2700 5600 2850
Wire Wire Line
	5500 2700 5500 2850
Wire Wire Line
	5400 2700 5400 2850
Wire Wire Line
	5300 2700 5300 2850
Wire Wire Line
	5200 2700 5200 2850
Wire Wire Line
	6300 1200 6300 1350
Wire Wire Line
	6200 1200 6200 1350
Wire Wire Line
	6100 1200 6100 1350
Wire Wire Line
	6000 1200 6000 1350
Wire Wire Line
	5900 1200 5900 1350
Wire Wire Line
	5800 1200 5800 1350
Wire Wire Line
	5700 1200 5700 1350
Wire Wire Line
	5600 1200 5600 1350
Wire Wire Line
	5600 3850 5600 4000
Wire Wire Line
	5700 3850 5700 4000
Wire Wire Line
	5800 3850 5800 4000
Wire Wire Line
	5900 3850 5900 4000
Wire Wire Line
	6000 3850 6000 4000
Wire Wire Line
	6100 3850 6100 4000
Wire Wire Line
	6200 3850 6200 4000
Wire Wire Line
	6300 3850 6300 4000
Entry Wire Line
	4550 3900 4650 4000
Wire Wire Line
	5400 4000 5400 3850
Wire Wire Line
	4650 4000 5400 4000
Text Label 4650 4000 0    60   ~ 0
~ROMEN
NoConn ~ 4550 4100
$Comp
L GND #PWR069
U 1 1 5621D759
P 5100 1350
F 0 "#PWR069" H 5100 1100 50  0001 C CNN
F 1 "GND" H 5100 1200 50  0000 C CNN
F 2 "" H 5100 1350 60  0000 C CNN
F 3 "" H 5100 1350 60  0000 C CNN
	1    5100 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 1350 5300 1350
Wire Wire Line
	5000 1350 5000 3850
Wire Wire Line
	5000 3850 5300 3850
Wire Wire Line
	5400 1350 5400 1300
Wire Wire Line
	5400 1300 4950 1300
Wire Wire Line
	4950 1300 4950 4000
Connection ~ 4950 4000
Text Label 6300 1350 1    60   ~ 0
D7
Text Label 6200 1350 1    60   ~ 0
D6
Text Label 6100 1350 1    60   ~ 0
D5
Text Label 6000 1350 1    60   ~ 0
D4
Text Label 5900 1350 1    60   ~ 0
D3
Text Label 5800 1350 1    60   ~ 0
D2
Text Label 5700 1350 1    60   ~ 0
D1
Text Label 5600 1350 1    60   ~ 0
D0
Text Label 5600 3850 3    60   ~ 0
D8
Text Label 5700 3850 3    60   ~ 0
D9
Text Label 5800 3850 3    60   ~ 0
D10
Text Label 5900 3850 3    60   ~ 0
D11
Text Label 6000 3850 3    60   ~ 0
D12
Text Label 6100 3850 3    60   ~ 0
D13
Text Label 6200 3850 3    60   ~ 0
D14
Text Label 6300 3850 3    60   ~ 0
D15
Text Label 6300 2350 3    60   ~ 0
A14
Text Label 6200 2350 3    60   ~ 0
A13
Text Label 6100 2350 3    60   ~ 0
A12
Text Label 6000 2350 3    60   ~ 0
A11
Text Label 5900 2350 3    60   ~ 0
A10
Text Label 5800 2350 3    60   ~ 0
A9
Text Label 5700 2350 3    60   ~ 0
A8
Text Label 5600 2350 3    60   ~ 0
A7
Text Label 5500 2350 3    60   ~ 0
A6
Text Label 5400 2350 3    60   ~ 0
A5
Text Label 5300 2350 3    60   ~ 0
A4
Text Label 5200 2350 3    60   ~ 0
A3
Text Label 6300 2850 1    60   ~ 0
A14
Text Label 6200 2850 1    60   ~ 0
A13
Text Label 6100 2850 1    60   ~ 0
A12
Text Label 6000 2850 1    60   ~ 0
A11
Text Label 5900 2850 1    60   ~ 0
A10
Text Label 5800 2850 1    60   ~ 0
A9
Text Label 5700 2850 1    60   ~ 0
A8
Text Label 5600 2850 1    60   ~ 0
A7
Text Label 5500 2850 1    60   ~ 0
A6
Text Label 5400 2850 1    60   ~ 0
A5
Text Label 5300 2850 1    60   ~ 0
A4
Text Label 5200 2850 1    60   ~ 0
A3
Wire Wire Line
	4450 2800 4250 2800
$EndSCHEMATC
