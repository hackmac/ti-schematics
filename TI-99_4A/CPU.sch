EESchema Schematic File Version 2
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 10
Title "TI-99/4A"
Date "2015-09-28"
Rev "1"
Comp "HackMac"
Comment1 "CPU and Clock Generator"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TMS9900 U600
U 1 1 5607B4DB
P 4700 4450
AR Path="/5607B4DB" Ref="U600"  Part="1" 
AR Path="/56085F00/5607B4DB" Ref="U600"  Part="1" 
F 0 "U600" H 5400 6200 60  0000 R CNN
F 1 "TMS9900" V 4700 4450 60  0000 C CNN
F 2 "Sockets_DIP:DIP-64__900" H 4700 4450 60  0001 C CNN
F 3 "" H 4700 4450 60  0000 C CNN
	1    4700 4450
	1    0    0    -1  
$EndComp
Entry Wire Line
	3450 4700 3550 4600
Entry Wire Line
	3450 4800 3550 4700
Entry Wire Line
	3450 4900 3550 4800
Entry Wire Line
	3450 5000 3550 4900
Entry Wire Line
	3450 5100 3550 5000
Entry Wire Line
	3450 5200 3550 5100
Entry Wire Line
	3450 5300 3550 5200
Entry Wire Line
	3450 5400 3550 5300
Entry Wire Line
	3450 5500 3550 5400
Entry Wire Line
	3450 5600 3550 5500
Entry Wire Line
	3450 5700 3550 5600
Entry Wire Line
	3450 5800 3550 5700
Entry Wire Line
	3450 5900 3550 5800
Entry Wire Line
	3450 6000 3550 5900
Entry Wire Line
	3450 6100 3550 6000
Entry Wire Line
	3350 4800 3450 4700
Entry Wire Line
	3350 4900 3450 4800
Entry Wire Line
	3350 5000 3450 4900
Entry Wire Line
	3350 5100 3450 5000
Entry Wire Line
	3350 5200 3450 5100
Entry Wire Line
	3350 5300 3450 5200
Entry Wire Line
	3350 5400 3450 5300
Entry Wire Line
	3350 5500 3450 5400
Entry Wire Line
	3350 5600 3450 5500
Entry Wire Line
	3350 5700 3450 5600
Entry Wire Line
	3350 5800 3450 5700
Entry Wire Line
	3350 5900 3450 5800
Entry Wire Line
	3350 6000 3450 5900
Entry Wire Line
	3350 6100 3450 6000
Entry Wire Line
	3350 6200 3450 6100
Text HLabel 3150 6200 0    60   Output ~ 0
U600-10
Text HLabel 3150 6100 0    60   Output ~ 0
U600-11
Text HLabel 3150 6000 0    60   Output ~ 0
U600-12
Text HLabel 3150 5900 0    60   Output ~ 0
U600-13
Text HLabel 3150 5800 0    60   Output ~ 0
U600-14
Text HLabel 3150 5700 0    60   Output ~ 0
U600-15
Text HLabel 3150 5600 0    60   Output ~ 0
U600-16
Text HLabel 3150 5500 0    60   Output ~ 0
U600-17
Text HLabel 3150 5400 0    60   Output ~ 0
U600-18
Text HLabel 3150 5300 0    60   Output ~ 0
U600-19
Text HLabel 3150 5200 0    60   Output ~ 0
U600-20
Text HLabel 3150 5100 0    60   Output ~ 0
U600-21
Text HLabel 3150 5000 0    60   Output ~ 0
U600-22
Text HLabel 3150 4900 0    60   Output ~ 0
U600-23
Text HLabel 3150 4800 0    60   Output ~ 0
U600-24
Text Label 3600 4600 0    60   ~ 0
A0
Text Label 3600 4700 0    60   ~ 0
A1
Text Label 3600 4800 0    60   ~ 0
A2
Text Label 3600 4900 0    60   ~ 0
A3
Text Label 3600 5000 0    60   ~ 0
A4
Text Label 3600 5100 0    60   ~ 0
A5
Text Label 3600 5200 0    60   ~ 0
A6
Text Label 3600 5300 0    60   ~ 0
A7
Text Label 3600 5400 0    60   ~ 0
A8
Text Label 3600 5500 0    60   ~ 0
A9
Text Label 3600 5600 0    60   ~ 0
A10
Text Label 3600 5700 0    60   ~ 0
A11
Text Label 3600 5800 0    60   ~ 0
A12
Text Label 3600 5900 0    60   ~ 0
A13
Text Label 3600 6000 0    60   ~ 0
A14
Text Label 3200 4800 0    60   ~ 0
A0
Text Label 3200 4900 0    60   ~ 0
A1
Text Label 3200 5000 0    60   ~ 0
A2
Text Label 3200 5100 0    60   ~ 0
A3
Text Label 3200 5200 0    60   ~ 0
A4
Text Label 3200 5300 0    60   ~ 0
A5
Text Label 3200 5400 0    60   ~ 0
A6
Text Label 3200 5500 0    60   ~ 0
A7
Text Label 3200 5600 0    60   ~ 0
A8
Text Label 3200 5700 0    60   ~ 0
A9
Text Label 3200 5800 0    60   ~ 0
A10
Text Label 3200 5900 0    60   ~ 0
A11
Text Label 3200 6000 0    60   ~ 0
A12
Text Label 3200 6100 0    60   ~ 0
A13
Text Label 3200 6200 0    60   ~ 0
A14
Entry Wire Line
	5850 4500 5950 4600
Entry Wire Line
	5850 4600 5950 4700
Entry Wire Line
	5850 4700 5950 4800
Entry Wire Line
	5850 4800 5950 4900
Entry Wire Line
	5850 4900 5950 5000
Entry Wire Line
	5850 5000 5950 5100
Entry Wire Line
	5850 5100 5950 5200
Entry Wire Line
	5850 5200 5950 5300
Entry Wire Line
	5850 5300 5950 5400
Entry Wire Line
	5850 5400 5950 5500
Entry Wire Line
	5850 5500 5950 5600
Entry Wire Line
	5850 5600 5950 5700
Entry Wire Line
	5850 5700 5950 5800
Entry Wire Line
	5850 5800 5950 5900
Entry Wire Line
	5850 5900 5950 6000
Entry Wire Line
	5850 6000 5950 6100
Entry Wire Line
	5950 4600 6050 4700
Entry Wire Line
	5950 4700 6050 4800
Entry Wire Line
	5950 4800 6050 4900
Entry Wire Line
	5950 4900 6050 5000
Entry Wire Line
	5950 5000 6050 5100
Entry Wire Line
	5950 5100 6050 5200
Entry Wire Line
	5950 5200 6050 5300
Entry Wire Line
	5950 5300 6050 5400
Entry Wire Line
	5950 5400 6050 5500
Entry Wire Line
	5950 5500 6050 5600
Entry Wire Line
	5950 5600 6050 5700
Entry Wire Line
	5950 5700 6050 5800
Entry Wire Line
	5950 5800 6050 5900
Entry Wire Line
	5950 5900 6050 6000
Entry Wire Line
	5950 6000 6050 6100
Entry Wire Line
	5950 6100 6050 6200
Text HLabel 6250 4700 2    60   BiDi ~ 0
U600-41
Text HLabel 6250 4800 2    60   BiDi ~ 0
U600-42
Text HLabel 6250 4900 2    60   BiDi ~ 0
U600-43
Text HLabel 6250 5000 2    60   BiDi ~ 0
U600-44
Text HLabel 6250 5100 2    60   BiDi ~ 0
U600-45
Text HLabel 6250 5200 2    60   BiDi ~ 0
U600-46
Text HLabel 6250 5300 2    60   BiDi ~ 0
U600-47
Text HLabel 6250 5400 2    60   BiDi ~ 0
U600-48
Text HLabel 6250 5500 2    60   BiDi ~ 0
U600-49
Text HLabel 6250 5600 2    60   BiDi ~ 0
U600-50
Text HLabel 6250 5700 2    60   BiDi ~ 0
U600-51
Text HLabel 6250 5800 2    60   BiDi ~ 0
U600-52
Text HLabel 6250 5900 2    60   BiDi ~ 0
U600-53
Text HLabel 6250 6000 2    60   BiDi ~ 0
U600-54
Text HLabel 6250 6100 2    60   BiDi ~ 0
U600-55
Text HLabel 6250 6200 2    60   BiDi ~ 0
U600-56
Text Label 5700 4500 0    60   ~ 0
D0
Text Label 5700 4600 0    60   ~ 0
D1
Text Label 5700 4700 0    60   ~ 0
D2
Text Label 5700 4800 0    60   ~ 0
D3
Text Label 5700 4900 0    60   ~ 0
D4
Text Label 5700 5000 0    60   ~ 0
D5
Text Label 5700 5100 0    60   ~ 0
D6
Text Label 5700 5200 0    60   ~ 0
D7
Text Label 5700 5300 0    60   ~ 0
D8
Text Label 5700 5400 0    60   ~ 0
D9
Text Label 5700 5500 0    60   ~ 0
D10
Text Label 5700 5600 0    60   ~ 0
D11
Text Label 5700 5700 0    60   ~ 0
D12
Text Label 5700 5800 0    60   ~ 0
D13
Text Label 5700 5900 0    60   ~ 0
D14
Text Label 5700 6000 0    60   ~ 0
D15
Text Label 6050 4700 0    60   ~ 0
D0
Text Label 6050 4800 0    60   ~ 0
D1
Text Label 6050 4900 0    60   ~ 0
D2
Text Label 6050 5000 0    60   ~ 0
D3
Text Label 6050 5100 0    60   ~ 0
D4
Text Label 6050 5200 0    60   ~ 0
D5
Text Label 6050 5300 0    60   ~ 0
D6
Text Label 6050 5400 0    60   ~ 0
D7
Text Label 6050 5500 0    60   ~ 0
D8
Text Label 6050 5600 0    60   ~ 0
D9
Text Label 6050 5700 0    60   ~ 0
D10
Text Label 6050 5800 0    60   ~ 0
D11
Text Label 6050 5900 0    60   ~ 0
D12
Text Label 6050 6000 0    60   ~ 0
D13
Text Label 6050 6100 0    60   ~ 0
D14
Text Label 6050 6200 0    60   ~ 0
D15
$Comp
L 74LS04 U602
U 1 1 560B367D
P 6350 4300
F 0 "U602" H 6545 4415 60  0000 C CNN
F 1 "74LS04" H 6540 4175 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 6350 4300 60  0001 C CNN
F 3 "" H 6350 4300 60  0000 C CNN
	1    6350 4300
	1    0    0    -1  
$EndComp
Text HLabel 7800 4300 2    60   Output ~ 0
U602-2
Text Label 7150 4300 2    60   ~ 0
DBIN
Text HLabel 7800 3100 2    60   Output ~ 0
U600-60
Text HLabel 7800 2900 2    60   Output ~ 0
U600-30
Text HLabel 3450 3550 0    60   Input ~ 0
U600-4
Text HLabel 7800 3000 2    60   Input ~ 0
U600-31
$Comp
L 74LS32 U605
U 1 1 56111EC5
P 6950 3950
F 0 "U605" H 6950 4000 60  0000 C CNN
F 1 "74LS32" H 6950 3900 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 6950 3950 60  0001 C CNN
F 3 "" H 6950 3950 60  0000 C CNN
	1    6950 3950
	1    0    0    -1  
$EndComp
Text HLabel 7800 3950 2    60   Output ~ 0
U605-3
$Comp
L GND #PWR014
U 1 1 561122B8
P 6350 3850
F 0 "#PWR014" H 6350 3600 50  0001 C CNN
F 1 "GND" H 6350 3700 50  0000 C CNN
F 2 "" H 6350 3850 60  0000 C CNN
F 3 "" H 6350 3850 60  0000 C CNN
	1    6350 3850
	1    0    0    -1  
$EndComp
Text HLabel 7800 3300 2    60   Output ~ 0
U600-5
Text HLabel 7800 3550 2    60   Output ~ 0
U600-7
Text HLabel 7800 3650 2    60   Output ~ 0
U600-61
Text HLabel 3650 2400 2    60   Output ~ 0
U601-14
Text Label 3600 2400 2    60   ~ 0
~Ø1
Text HLabel 3650 2600 2    60   Output ~ 0
U601-7
Text Label 3600 2600 2    60   ~ 0
~Ø3
$Comp
L TIM9904 U601
U 1 1 5609182B
P 2700 2800
F 0 "U601" H 3100 3450 60  0000 R CNN
F 1 "TIM9904" H 2700 2800 60  0000 C CNN
F 2 "Sockets_DIP:DIP-20__300" H 2700 2800 60  0001 C CNN
F 3 "" H 2700 2800 60  0000 C CNN
	1    2700 2800
	1    0    0    -1  
$EndComp
Text Label 3600 2500 2    60   ~ 0
~Ø2
Text Label 3600 2700 2    60   ~ 0
~Ø4
Text HLabel 3650 2500 2    60   Output ~ 0
U601-15
Text HLabel 3650 2700 2    60   Output ~ 0
U601-6
$Comp
L R R600
U 1 1 56098107
P 3500 2900
F 0 "R600" V 3500 2900 50  0000 C CNN
F 1 "22" V 3450 3050 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 3430 2900 30  0001 C CNN
F 3 "" H 3500 2900 30  0000 C CNN
	1    3500 2900
	0    1    1    0   
$EndComp
$Comp
L R R601
U 1 1 56098128
P 3500 3000
F 0 "R601" V 3500 3000 50  0000 C CNN
F 1 "22" V 3450 3150 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 3430 3000 30  0001 C CNN
F 3 "" H 3500 3000 30  0000 C CNN
	1    3500 3000
	0    1    1    0   
$EndComp
$Comp
L R R602
U 1 1 5609814D
P 3500 3100
F 0 "R602" V 3500 3100 50  0000 C CNN
F 1 "22" V 3450 3250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 3430 3100 30  0001 C CNN
F 3 "" H 3500 3100 30  0000 C CNN
	1    3500 3100
	0    1    1    0   
$EndComp
$Comp
L R R603
U 1 1 5609816E
P 3500 3200
F 0 "R603" V 3500 3200 50  0000 C CNN
F 1 "22" V 3450 3350 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 3430 3200 30  0001 C CNN
F 3 "" H 3500 3200 30  0000 C CNN
	1    3500 3200
	0    1    1    0   
$EndComp
$Comp
L GND #PWR015
U 1 1 56098B69
P 3550 3800
F 0 "#PWR015" H 3550 3550 50  0001 C CNN
F 1 "GND" H 3550 3650 50  0000 C CNN
F 2 "" H 3550 3800 60  0000 C CNN
F 3 "" H 3550 3800 60  0000 C CNN
	1    3550 3800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR016
U 1 1 56098B95
P 3550 4100
F 0 "#PWR016" H 3550 3950 50  0001 C CNN
F 1 "+5V" H 3550 4240 50  0000 C CNN
F 2 "" H 3550 4100 60  0000 C CNN
F 3 "" H 3550 4100 60  0000 C CNN
	1    3550 4100
	0    -1   -1   0   
$EndComp
Text HLabel 3150 4200 0    60   Input ~ 0
U600-32
$Comp
L Crystal Y600
U 1 1 560AF6CA
P 1800 2900
F 0 "Y600" H 1800 3050 50  0000 C CNN
F 1 "48,0000 MHz" H 1800 2750 50  0000 C CNN
F 2 "Crystals:HC-18UV" H 1800 2900 60  0001 C CNN
F 3 "" H 1800 2900 60  0000 C CNN
	1    1800 2900
	1    0    0    -1  
$EndComp
Text HLabel 2950 1300 2    60   Input ~ 0
RESET
$Comp
L R R514
U 1 1 560AFBCA
P 2550 1450
F 0 "R514" V 2550 1450 50  0000 C CNN
F 1 "47k" V 2600 1600 50  0000 L CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 2480 1450 30  0001 C CNN
F 3 "" H 2550 1450 30  0000 C CNN
	1    2550 1450
	0    1    1    0   
$EndComp
$Comp
L CP1 C506
U 1 1 560AFC0F
P 2550 1150
F 0 "C506" V 2700 1150 50  0000 C CNN
F 1 "22µ" V 2500 1300 50  0000 L CNN
F 2 "" H 2550 1150 60  0001 C CNN
F 3 "" H 2550 1150 60  0000 C CNN
	1    2550 1150
	0    1    1    0   
$EndComp
$Comp
L R R605
U 1 1 560B0833
P 1950 1300
F 0 "R605" V 1950 1300 50  0000 C CNN
F 1 "12k" H 2000 1450 50  0000 L CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 1880 1300 30  0001 C CNN
F 3 "" H 1950 1300 30  0000 C CNN
	1    1950 1300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR017
U 1 1 560B08F2
P 1950 1150
F 0 "#PWR017" H 1950 1000 50  0001 C CNN
F 1 "+5V" H 1950 1290 50  0000 C CNN
F 2 "" H 1950 1150 60  0000 C CNN
F 3 "" H 1950 1150 60  0000 C CNN
	1    1950 1150
	1    0    0    -1  
$EndComp
$Comp
L R R606
U 1 1 560B0A6C
P 1400 1450
F 0 "R606" V 1400 1450 50  0000 C CNN
F 1 "150k" V 1450 1600 50  0000 L CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 1330 1450 30  0001 C CNN
F 3 "" H 1400 1450 30  0000 C CNN
	1    1400 1450
	0    1    1    0   
$EndComp
$Comp
L CP1 C606
U 1 1 560B0ACE
P 1400 1150
F 0 "C606" V 1550 1150 50  0000 C CNN
F 1 "22µ" V 1350 1300 50  0000 L CNN
F 2 "" H 1400 1150 60  0001 C CNN
F 3 "" H 1400 1150 60  0000 C CNN
	1    1400 1150
	0    1    1    0   
$EndComp
$Comp
L GND #PWR018
U 1 1 560B0CC5
P 1150 1450
F 0 "#PWR018" H 1150 1200 50  0001 C CNN
F 1 "GND" H 1150 1300 50  0000 C CNN
F 2 "" H 1150 1450 60  0000 C CNN
F 3 "" H 1150 1450 60  0000 C CNN
	1    1150 1450
	1    0    0    -1  
$EndComp
$Comp
L CP1 C603
U 1 1 560B1A73
P 1350 2250
F 0 "C603" H 1375 2350 50  0000 L CNN
F 1 "22p" H 1375 2150 50  0000 L CNN
F 2 "" H 1350 2250 60  0001 C CNN
F 3 "" H 1350 2250 60  0000 C CNN
	1    1350 2250
	1    0    0    -1  
$EndComp
$Comp
L R R604
U 1 1 560B28F2
P 1800 3200
F 0 "R604" V 1800 3200 50  0000 C CNN
F 1 "1k" V 1750 3350 50  0000 L CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 1730 3200 30  0001 C CNN
F 3 "" H 1800 3200 30  0000 C CNN
	1    1800 3200
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR019
U 1 1 560B2BB3
P 3150 1850
F 0 "#PWR019" H 3150 1700 50  0001 C CNN
F 1 "+5V" H 3150 1990 50  0000 C CNN
F 2 "" H 3150 1850 60  0000 C CNN
F 3 "" H 3150 1850 60  0000 C CNN
	1    3150 1850
	1    0    0    -1  
$EndComp
$Comp
L L_Small L604
U 1 1 560B2E8F
P 2950 1950
F 0 "L604" H 2980 1990 50  0000 L CNN
F 1 "6.8" H 2980 1910 50  0000 L CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" H 2950 1950 60  0001 C CNN
F 3 "" H 2950 1950 60  0000 C CNN
	1    2950 1950
	0    -1   -1   0   
$EndComp
$Comp
L CP1 C607
U 1 1 560B3485
P 1050 2100
F 0 "C607" H 950 2200 50  0000 R CNN
F 1 "0.001" H 950 2000 50  0000 R CNN
F 2 "" H 1050 2100 60  0001 C CNN
F 3 "" H 1050 2100 60  0000 C CNN
	1    1050 2100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 560B3581
P 1050 2350
F 0 "#PWR020" H 1050 2100 50  0001 C CNN
F 1 "GND" H 1050 2200 50  0000 C CNN
F 2 "" H 1050 2350 60  0000 C CNN
F 3 "" H 1050 2350 60  0000 C CNN
	1    1050 2350
	1    0    0    -1  
$EndComp
$Comp
L CP1 C605
U 1 1 560B3C51
P 3400 1950
F 0 "C605" H 3425 2050 50  0000 L CNN
F 1 "0.1" H 3425 1850 50  0000 L CNN
F 2 "" H 3400 1950 60  0001 C CNN
F 3 "" H 3400 1950 60  0000 C CNN
	1    3400 1950
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR021
U 1 1 560B3D14
P 3700 1950
F 0 "#PWR021" H 3700 1700 50  0001 C CNN
F 1 "GND" H 3700 1800 50  0000 C CNN
F 2 "" H 3700 1950 60  0000 C CNN
F 3 "" H 3700 1950 60  0000 C CNN
	1    3700 1950
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR022
U 1 1 560B492D
P 4200 1350
F 0 "#PWR022" H 4200 1200 50  0001 C CNN
F 1 "+12V" H 4200 1490 50  0000 C CNN
F 2 "" H 4200 1350 60  0000 C CNN
F 3 "" H 4200 1350 60  0000 C CNN
	1    4200 1350
	1    0    0    -1  
$EndComp
$Comp
L CP1 C604
U 1 1 560B497D
P 2450 1750
F 0 "C604" H 2475 1850 50  0000 L CNN
F 1 "0.1" H 2475 1650 50  0000 L CNN
F 2 "" H 2450 1750 60  0001 C CNN
F 3 "" H 2450 1750 60  0000 C CNN
	1    2450 1750
	0    1    1    0   
$EndComp
$Comp
L GND #PWR023
U 1 1 560B49E1
P 2150 1750
F 0 "#PWR023" H 2150 1500 50  0001 C CNN
F 1 "GND" H 2150 1600 50  0000 C CNN
F 2 "" H 2150 1750 60  0000 C CNN
F 3 "" H 2150 1750 60  0000 C CNN
	1    2150 1750
	1    0    0    -1  
$EndComp
Wire Bus Line
	3450 4700 3450 6100
Wire Wire Line
	3550 4600 3700 4600
Wire Wire Line
	3550 4700 3700 4700
Wire Wire Line
	3550 4800 3700 4800
Wire Wire Line
	3550 4900 3700 4900
Wire Wire Line
	3550 5000 3700 5000
Wire Wire Line
	3550 5100 3700 5100
Wire Wire Line
	3550 5200 3700 5200
Wire Wire Line
	3550 5300 3700 5300
Wire Wire Line
	3550 5400 3700 5400
Wire Wire Line
	3550 5500 3700 5500
Wire Wire Line
	3550 5600 3700 5600
Wire Wire Line
	3550 5700 3700 5700
Wire Wire Line
	3550 5800 3700 5800
Wire Wire Line
	3550 5900 3700 5900
Wire Wire Line
	3550 6000 3700 6000
Wire Wire Line
	3150 4800 3350 4800
Wire Wire Line
	3150 4900 3350 4900
Wire Wire Line
	3150 5000 3350 5000
Wire Wire Line
	3150 5100 3350 5100
Wire Wire Line
	3150 5200 3350 5200
Wire Wire Line
	3150 5300 3350 5300
Wire Wire Line
	3150 5400 3350 5400
Wire Wire Line
	3150 5500 3350 5500
Wire Wire Line
	3150 5600 3350 5600
Wire Wire Line
	3150 5700 3350 5700
Wire Wire Line
	3150 5800 3350 5800
Wire Wire Line
	3150 5900 3350 5900
Wire Wire Line
	3150 6000 3350 6000
Wire Wire Line
	3150 6100 3350 6100
Wire Wire Line
	3150 6200 3350 6200
Wire Bus Line
	5950 4600 5950 6100
Wire Wire Line
	5700 4500 5850 4500
Wire Wire Line
	5700 4600 5850 4600
Wire Wire Line
	5700 4700 5850 4700
Wire Wire Line
	5700 4800 5850 4800
Wire Wire Line
	5700 4900 5850 4900
Wire Wire Line
	5700 5000 5850 5000
Wire Wire Line
	5700 5100 5850 5100
Wire Wire Line
	5700 5200 5850 5200
Wire Wire Line
	5700 5300 5850 5300
Wire Wire Line
	5700 5400 5850 5400
Wire Wire Line
	5700 5500 5850 5500
Wire Wire Line
	5700 5600 5850 5600
Wire Wire Line
	5700 5700 5850 5700
Wire Wire Line
	5700 5800 5850 5800
Wire Wire Line
	5700 5900 5850 5900
Wire Wire Line
	5700 6000 5850 6000
Wire Wire Line
	6050 4700 6250 4700
Wire Wire Line
	6050 4800 6250 4800
Wire Wire Line
	6050 4900 6250 4900
Wire Wire Line
	6050 5000 6250 5000
Wire Wire Line
	6050 5100 6250 5100
Wire Wire Line
	6050 5200 6250 5200
Wire Wire Line
	6050 5300 6250 5300
Wire Wire Line
	6050 5400 6250 5400
Wire Wire Line
	6050 5500 6250 5500
Wire Wire Line
	6050 5600 6250 5600
Wire Wire Line
	6050 5700 6250 5700
Wire Wire Line
	6050 5800 6250 5800
Wire Wire Line
	6050 5900 6250 5900
Wire Wire Line
	6050 6000 6250 6000
Wire Wire Line
	6050 6100 6250 6100
Wire Wire Line
	6050 6200 6250 6200
Wire Wire Line
	5700 4300 5900 4300
Wire Wire Line
	6800 4300 7800 4300
Wire Wire Line
	7800 3100 5700 3100
Wire Wire Line
	5700 2900 7800 2900
Wire Wire Line
	3700 3550 3450 3550
Wire Wire Line
	5700 3000 7800 3000
Wire Wire Line
	5700 4200 6100 4200
Wire Wire Line
	6100 4200 6100 4050
Wire Wire Line
	6100 4050 6350 4050
Wire Wire Line
	7550 3950 7800 3950
Wire Wire Line
	5700 3900 5900 3900
Wire Wire Line
	5900 3900 5900 3550
Wire Wire Line
	5900 3550 7800 3550
Wire Wire Line
	5700 3300 7800 3300
Wire Wire Line
	5700 4100 6000 4100
Wire Wire Line
	6000 4100 6000 3650
Wire Wire Line
	6000 3650 7800 3650
Wire Wire Line
	3300 2400 3650 2400
Wire Wire Line
	3300 2600 3650 2600
Wire Wire Line
	3300 2500 3650 2500
Wire Wire Line
	3300 2700 3650 2700
Wire Wire Line
	1300 3650 3700 3650
Wire Wire Line
	1500 3650 1500 2600
Wire Wire Line
	1500 2600 2050 2600
Wire Wire Line
	3300 2900 3350 2900
Wire Wire Line
	3300 3000 3350 3000
Wire Wire Line
	3300 3100 3350 3100
Wire Wire Line
	3300 3200 3350 3200
Wire Wire Line
	3650 2900 3700 2900
Wire Wire Line
	3650 3000 3700 3000
Wire Wire Line
	3650 3100 3700 3100
Wire Wire Line
	3650 3200 3700 3200
Wire Wire Line
	3700 3800 3700 4000
Connection ~ 3700 3900
Wire Wire Line
	3550 3800 3700 3800
Wire Wire Line
	3550 4100 3700 4100
Wire Wire Line
	3700 4200 3150 4200
Wire Wire Line
	1950 2900 2050 2900
Wire Wire Line
	1650 2900 1650 3000
Wire Wire Line
	1650 3000 2050 3000
Wire Wire Line
	2700 1150 2800 1150
Wire Wire Line
	2800 1150 2800 1450
Wire Wire Line
	2800 1450 2700 1450
Wire Wire Line
	2950 1300 2800 1300
Connection ~ 2800 1300
Wire Wire Line
	2400 1150 2300 1150
Wire Wire Line
	2300 1150 2300 1450
Wire Wire Line
	1550 1450 2400 1450
Wire Wire Line
	1950 1450 1950 2700
Wire Wire Line
	1950 2700 2050 2700
Connection ~ 2300 1450
Wire Wire Line
	1550 1150 1650 1150
Wire Wire Line
	1650 1150 1650 1450
Connection ~ 1950 1450
Connection ~ 1650 1450
Wire Wire Line
	1250 1150 1150 1150
Wire Wire Line
	1150 1150 1150 1450
Wire Wire Line
	1150 1450 1250 1450
Connection ~ 1150 1450
Wire Wire Line
	1350 2450 1900 2450
Wire Wire Line
	1350 2450 1350 2400
Connection ~ 1600 2450
Wire Wire Line
	1350 2100 1350 2050
Wire Wire Line
	1350 2050 1900 2050
Wire Wire Line
	1900 2050 1900 2300
Wire Wire Line
	1900 2300 2050 2300
Connection ~ 1600 2050
Wire Wire Line
	1950 3200 2050 3200
Wire Wire Line
	3050 1950 3250 1950
Wire Wire Line
	1050 1950 2850 1950
Wire Wire Line
	2800 2000 2800 1950
Connection ~ 2800 1950
Wire Wire Line
	2050 2400 1900 2400
Wire Wire Line
	1900 2400 1900 2450
Wire Wire Line
	1200 1950 1200 3200
Wire Wire Line
	1200 3200 1650 3200
Connection ~ 1200 1950
Wire Wire Line
	1050 2250 1050 2350
Wire Wire Line
	3150 1950 3150 1850
Connection ~ 3150 1950
Wire Wire Line
	3550 1950 3700 1950
Wire Wire Line
	2300 1750 2150 1750
Connection ~ 2600 1750
Wire Wire Line
	2600 1750 4900 1750
Wire Wire Line
	2600 1750 2600 2000
$Comp
L L_Small L603
U 1 1 560B52C7
P 4200 1550
F 0 "L603" H 4230 1590 50  0000 L CNN
F 1 "6.8" H 4230 1510 50  0000 L CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" H 4200 1550 60  0001 C CNN
F 3 "" H 4200 1550 60  0000 C CNN
	1    4200 1550
	1    0    0    -1  
$EndComp
$Comp
L L_Small L602
U 1 1 560B5336
P 1600 2250
F 0 "L602" H 1630 2290 50  0000 L CNN
F 1 "33µH" H 1630 2210 50  0000 L CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" H 1600 2250 60  0001 C CNN
F 3 "" H 1600 2250 60  0000 C CNN
	1    1600 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 2050 1600 2150
Wire Wire Line
	1600 2350 1600 2450
Wire Wire Line
	4200 1450 4200 1350
Wire Wire Line
	4900 1750 4900 2450
$Comp
L CP1 C612
U 1 1 560B6515
P 4200 1900
F 0 "C612" H 4225 2000 50  0000 L CNN
F 1 "0.1" H 4225 1800 50  0000 L CNN
F 2 "" H 4200 1900 60  0001 C CNN
F 3 "" H 4200 1900 60  0000 C CNN
	1    4200 1900
	1    0    0    -1  
$EndComp
Connection ~ 4200 1750
$Comp
L GND #PWR024
U 1 1 560B65A6
P 4200 2050
F 0 "#PWR024" H 4200 1800 50  0001 C CNN
F 1 "GND" H 4200 1900 50  0000 C CNN
F 2 "" H 4200 2050 60  0000 C CNN
F 3 "" H 4200 2050 60  0000 C CNN
	1    4200 2050
	1    0    0    -1  
$EndComp
$Comp
L R R608
U 1 1 560B6D1E
P 5950 3400
F 0 "R608" V 5950 3400 50  0000 C CNN
F 1 "1k" V 5900 3300 50  0000 R CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5880 3400 30  0001 C CNN
F 3 "" H 5950 3400 30  0000 C CNN
	1    5950 3400
	0    1    1    0   
$EndComp
$Comp
L +5V #PWR025
U 1 1 560B6D80
P 6200 3400
F 0 "#PWR025" H 6200 3250 50  0001 C CNN
F 1 "+5V" H 6200 3540 50  0000 C CNN
F 2 "" H 6200 3400 60  0000 C CNN
F 3 "" H 6200 3400 60  0000 C CNN
	1    6200 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 3400 6200 3400
Wire Wire Line
	5700 3400 5800 3400
Text HLabel 7800 3450 2    60   Input ~ 0
U600-62
Wire Wire Line
	7800 3450 6450 3450
Wire Wire Line
	6450 3450 6450 3500
Wire Wire Line
	6450 3500 5800 3500
Wire Wire Line
	5800 3500 5800 3600
Wire Wire Line
	5800 3600 5700 3600
Text HLabel 1300 3650 0    60   Output ~ 0
U601-4
Connection ~ 1500 3650
$Comp
L R R607
U 1 1 560B878B
P 5750 2600
F 0 "R607" V 5750 2600 50  0000 C CNN
F 1 "4.7k" V 5650 2600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5680 2600 30  0001 C CNN
F 3 "" H 5750 2600 30  0000 C CNN
	1    5750 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2750 5750 4200
Connection ~ 5750 4200
Wire Wire Line
	5750 2400 5750 2450
Wire Wire Line
	4650 2400 5750 2400
Wire Wire Line
	5150 2250 5150 2400
Wire Wire Line
	4650 2400 4650 2450
$Comp
L CP1 C611
U 1 1 560B8F49
P 5700 2300
F 0 "C611" H 5725 2400 50  0000 L CNN
F 1 "0.001" H 5725 2200 50  0000 L CNN
F 2 "" H 5700 2300 60  0001 C CNN
F 3 "" H 5700 2300 60  0000 C CNN
	1    5700 2300
	0    -1   -1   0   
$EndComp
$Comp
L CP1 C600
U 1 1 560B8FBE
P 5700 2000
F 0 "C600" H 5725 2100 50  0000 L CNN
F 1 "100" H 5725 1900 50  0000 L CNN
F 2 "" H 5700 2000 60  0001 C CNN
F 3 "" H 5700 2000 60  0000 C CNN
	1    5700 2000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4500 2300 4500 2450
Wire Wire Line
	5150 2300 5550 2300
$Comp
L GND #PWR026
U 1 1 560B9271
P 6400 2150
F 0 "#PWR026" H 6400 1900 50  0001 C CNN
F 1 "GND" H 6400 2000 50  0000 C CNN
F 2 "" H 6400 2150 60  0000 C CNN
F 3 "" H 6400 2150 60  0000 C CNN
	1    6400 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 2300 5850 2300
Wire Wire Line
	5950 2000 5950 2300
Wire Wire Line
	5950 2000 5850 2000
Wire Wire Line
	5950 2150 6400 2150
Connection ~ 5950 2150
$Comp
L L_Small L600
U 1 1 560B946B
P 5150 2150
F 0 "L600" H 5180 2190 50  0000 L CNN
F 1 "6.8" H 5180 2110 50  0000 L CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM15mm" H 5150 2150 60  0001 C CNN
F 3 "" H 5150 2150 60  0000 C CNN
	1    5150 2150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR027
U 1 1 560B95A3
P 5150 1950
F 0 "#PWR027" H 5150 1800 50  0001 C CNN
F 1 "+5V" H 5150 2090 50  0000 C CNN
F 2 "" H 5150 1950 60  0000 C CNN
F 3 "" H 5150 1950 60  0000 C CNN
	1    5150 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1950 5150 2050
Connection ~ 5150 2300
Wire Wire Line
	5550 2000 5150 2000
Connection ~ 5150 2000
Connection ~ 5150 2400
$Comp
L CP1 C602
U 1 1 560BA374
P 4500 2150
F 0 "C602" H 4525 2250 50  0000 L CNN
F 1 "0.01" H 4525 2050 50  0000 L CNN
F 2 "" H 4500 2150 60  0001 C CNN
F 3 "" H 4500 2150 60  0000 C CNN
	1    4500 2150
	-1   0    0    1   
$EndComp
$Comp
L -5V #PWR028
U 1 1 560BA4A6
P 4500 2350
F 0 "#PWR028" H 4500 2450 50  0001 C CNN
F 1 "-5V" H 4500 2500 50  0000 C CNN
F 2 "" H 4500 2350 60  0000 C CNN
F 3 "" H 4500 2350 60  0000 C CNN
	1    4500 2350
	0    -1   -1   0   
$EndComp
$Comp
L CP1 C601
U 1 1 560BA511
P 4750 2100
F 0 "C601" H 4775 2200 50  0000 L CNN
F 1 "CP1" H 4775 2000 50  0000 L CNN
F 2 "" H 4750 2100 60  0001 C CNN
F 3 "" H 4750 2100 60  0000 C CNN
	1    4750 2100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4750 2250 4750 2450
$Comp
L +5V #PWR029
U 1 1 560BA660
P 4750 2300
F 0 "#PWR029" H 4750 2150 50  0001 C CNN
F 1 "+5V" H 4750 2440 50  0000 C CNN
F 2 "" H 4750 2300 60  0000 C CNN
F 3 "" H 4750 2300 60  0000 C CNN
	1    4750 2300
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR030
U 1 1 560BA79A
P 4750 1950
F 0 "#PWR030" H 4750 1700 50  0001 C CNN
F 1 "GND" H 4750 1800 50  0000 C CNN
F 2 "" H 4750 1950 60  0000 C CNN
F 3 "" H 4750 1950 60  0000 C CNN
	1    4750 1950
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR031
U 1 1 560BA844
P 4500 2000
F 0 "#PWR031" H 4500 1750 50  0001 C CNN
F 1 "GND" H 4500 1850 50  0000 C CNN
F 2 "" H 4500 2000 60  0000 C CNN
F 3 "" H 4500 2000 60  0000 C CNN
	1    4500 2000
	-1   0    0    1   
$EndComp
Connection ~ 4750 2300
Connection ~ 4500 2350
Wire Wire Line
	4200 1750 4200 1650
$EndSCHEMATC
