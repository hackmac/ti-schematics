EESchema Schematic File Version 2
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 10
Title "TI-99/4A"
Date "2015-09-28"
Rev "1"
Comp "HackMac"
Comment1 "Logic for generating the Address line 15 and multiplexing the Databus"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74LS245 U614
U 1 1 5606B75F
P 2700 1600
F 0 "U614" H 2800 2175 60  0000 L BNN
F 1 "74LS245" H 2750 1025 60  0000 L TNN
F 2 "Sockets_DIP:DIP-20__300" H 2700 1600 60  0001 C CNN
F 3 "" H 2700 1600 60  0000 C CNN
	1    2700 1600
	1    0    0    -1  
$EndComp
$Comp
L 74LS373 U615
U 1 1 5606B7A8
P 2650 3350
F 0 "U615" H 2650 3350 60  0000 C CNN
F 1 "74LS373" H 2700 3000 60  0000 C CNN
F 2 "Sockets_DIP:DIP-20__300" H 2650 3350 60  0001 C CNN
F 3 "" H 2650 3350 60  0000 C CNN
	1    2650 3350
	-1   0    0    -1  
$EndComp
$Comp
L 74LS244 U616
U 1 1 5606B7C8
P 2650 4850
F 0 "U616" H 2700 4650 60  0000 C CNN
F 1 "74LS244" H 2750 4450 60  0000 C CNN
F 2 "Sockets_DIP:DIP-20__300" H 2650 4850 60  0001 C CNN
F 3 "" H 2650 4850 60  0000 C CNN
	1    2650 4850
	1    0    0    -1  
$EndComp
Entry Wire Line
	3600 1800 3700 1900
Entry Wire Line
	3600 1700 3700 1800
Entry Wire Line
	3600 1600 3700 1700
Entry Wire Line
	3600 1500 3700 1600
Entry Wire Line
	3600 1400 3700 1500
Entry Wire Line
	3600 1300 3700 1400
Entry Wire Line
	3600 1200 3700 1300
Entry Wire Line
	3600 1100 3700 1200
Entry Wire Line
	3600 2850 3700 2750
Entry Wire Line
	3600 2950 3700 2850
Entry Wire Line
	3600 3050 3700 2950
Entry Wire Line
	3600 3150 3700 3050
Entry Wire Line
	3600 3250 3700 3150
Entry Wire Line
	3600 3350 3700 3250
Entry Wire Line
	3600 3450 3700 3350
Entry Wire Line
	3600 3550 3700 3450
Entry Wire Line
	3600 4350 3700 4250
Entry Wire Line
	3600 4450 3700 4350
Entry Wire Line
	3600 4550 3700 4450
Entry Wire Line
	3600 4650 3700 4550
Entry Wire Line
	3600 4750 3700 4650
Entry Wire Line
	3600 4850 3700 4750
Entry Wire Line
	3600 4950 3700 4850
Entry Wire Line
	3600 5050 3700 4950
Text Label 3400 1800 0    60   ~ 0
oD0
Text Label 3400 1700 0    60   ~ 0
oD1
Text Label 3400 1600 0    60   ~ 0
oD2
Text Label 3400 1500 0    60   ~ 0
oD3
Text Label 3400 1400 0    60   ~ 0
oD4
Text Label 3400 1300 0    60   ~ 0
oD5
Text Label 3400 1200 0    60   ~ 0
oD6
Text Label 3400 1100 0    60   ~ 0
oD7
Text Label 3350 3150 0    60   ~ 0
oD0
Text Label 3350 3250 0    60   ~ 0
oD1
Text Label 3350 3050 0    60   ~ 0
oD2
Text Label 3350 3350 0    60   ~ 0
oD3
Text Label 3350 2950 0    60   ~ 0
oD4
Text Label 3350 3450 0    60   ~ 0
oD5
Text Label 3350 2850 0    60   ~ 0
oD6
Text Label 3350 3550 0    60   ~ 0
oD7
Text Label 3350 4750 0    60   ~ 0
oD0
Text Label 3350 4650 0    60   ~ 0
oD1
Text Label 3350 4850 0    60   ~ 0
oD2
Text Label 3350 4550 0    60   ~ 0
oD3
Text Label 3350 4950 0    60   ~ 0
oD4
Text Label 3350 4450 0    60   ~ 0
oD5
Text Label 3350 5050 0    60   ~ 0
oD6
Text Label 3350 4350 0    60   ~ 0
oD7
Entry Wire Line
	1600 2950 1700 2850
Entry Wire Line
	1600 3050 1700 2950
Entry Wire Line
	1600 3150 1700 3050
Entry Wire Line
	1600 3250 1700 3150
Entry Wire Line
	1600 3350 1700 3250
Entry Wire Line
	1600 3450 1700 3350
Entry Wire Line
	1600 3550 1700 3450
Entry Wire Line
	1600 3650 1700 3550
Entry Wire Line
	1600 4250 1700 4350
Entry Wire Line
	1600 4350 1700 4450
Entry Wire Line
	1600 4450 1700 4550
Entry Wire Line
	1600 4550 1700 4650
Entry Wire Line
	1600 4650 1700 4750
Entry Wire Line
	1600 4750 1700 4850
Entry Wire Line
	1600 4850 1700 4950
Entry Wire Line
	1600 4950 1700 5050
Entry Wire Line
	1500 1900 1600 2000
Entry Wire Line
	1500 2000 1600 2100
Entry Wire Line
	1500 2100 1600 2200
Entry Wire Line
	1500 2200 1600 2300
Entry Wire Line
	1500 2300 1600 2400
Entry Wire Line
	1500 2400 1600 2500
Entry Wire Line
	1500 2500 1600 2600
Entry Wire Line
	1500 2600 1600 2700
Text HLabel 1050 1900 0    60   BiDi ~ 0
U600-49
Text HLabel 1050 2000 0    60   BiDi ~ 0
U600-50
Text HLabel 1050 2100 0    60   BiDi ~ 0
U600-51
Text HLabel 1050 2200 0    60   BiDi ~ 0
U600-52
Text HLabel 1050 2300 0    60   BiDi ~ 0
U600-53
Text HLabel 1050 2400 0    60   BiDi ~ 0
U600-54
Text HLabel 1050 2500 0    60   BiDi ~ 0
U600-55
Text HLabel 1050 2600 0    60   BiDi ~ 0
U600-56
Text Label 1150 1900 0    60   ~ 0
cD8
Text Label 1150 2000 0    60   ~ 0
cD9
Text Label 1150 2100 0    60   ~ 0
cD10
Text Label 1150 2200 0    60   ~ 0
cD11
Text Label 1150 2300 0    60   ~ 0
cD12
Text Label 1150 2400 0    60   ~ 0
cD13
Text Label 1150 2500 0    60   ~ 0
cD14
Text Label 1150 2600 0    60   ~ 0
cD15
Text HLabel 1050 1100 0    60   BiDi ~ 0
U600-41
Text HLabel 1050 1200 0    60   BiDi ~ 0
U600-42
Text HLabel 1050 1300 0    60   BiDi ~ 0
U600-43
Text HLabel 1050 1400 0    60   BiDi ~ 0
U600-44
Text HLabel 1050 1500 0    60   BiDi ~ 0
U600-45
Text HLabel 1050 1600 0    60   BiDi ~ 0
U600-46
Text HLabel 1050 1700 0    60   BiDi ~ 0
U600-47
Text HLabel 1050 1800 0    60   BiDi ~ 0
U600-48
Entry Wire Line
	1500 1100 1600 1200
Entry Wire Line
	1500 1200 1600 1300
Entry Wire Line
	1500 1300 1600 1400
Entry Wire Line
	1500 1400 1600 1500
Entry Wire Line
	1500 1500 1600 1600
Entry Wire Line
	1500 1600 1600 1700
Entry Wire Line
	1500 1700 1600 1800
Entry Wire Line
	1500 1800 1600 1900
Text Label 1150 1100 0    60   ~ 0
cD0
Text Label 1150 1200 0    60   ~ 0
cD1
Text Label 1150 1300 0    60   ~ 0
cD2
Text Label 1150 1400 0    60   ~ 0
cD3
Text Label 1150 1500 0    60   ~ 0
cD4
Text Label 1150 1600 0    60   ~ 0
cD5
Text Label 1150 1700 0    60   ~ 0
cD6
Text Label 1150 1800 0    60   ~ 0
cD7
Entry Wire Line
	1600 1200 1700 1100
Entry Wire Line
	1600 1300 1700 1200
Entry Wire Line
	1600 1400 1700 1300
Entry Wire Line
	1600 1500 1700 1400
Entry Wire Line
	1600 1600 1700 1500
Entry Wire Line
	1600 1700 1700 1600
Entry Wire Line
	1600 1800 1700 1700
Entry Wire Line
	1600 1900 1700 1800
Text Label 1800 1800 0    60   ~ 0
cD0
Text Label 1800 1700 0    60   ~ 0
cD1
Text Label 1800 1600 0    60   ~ 0
cD2
Text Label 1800 1500 0    60   ~ 0
cD3
Text Label 1800 1400 0    60   ~ 0
cD4
Text Label 1800 1300 0    60   ~ 0
cD5
Text Label 1800 1200 0    60   ~ 0
cD6
Text Label 1800 1100 0    60   ~ 0
cD7
Text Label 1750 3150 0    60   ~ 0
cD8
Text Label 1750 3250 0    60   ~ 0
cD9
Text Label 1750 3050 0    60   ~ 0
cD10
Text Label 1750 3350 0    60   ~ 0
cD11
Text Label 1750 2950 0    60   ~ 0
cD12
Text Label 1750 3450 0    60   ~ 0
cD13
Text Label 1750 3550 0    60   ~ 0
cD14
Text Label 1750 2850 0    60   ~ 0
cD15
Text Label 1750 4750 0    60   ~ 0
cD8
Text Label 1750 4650 0    60   ~ 0
cD9
Text Label 1750 4850 0    60   ~ 0
cD10
Text Label 1750 4550 0    60   ~ 0
cD11
Text Label 1750 4950 0    60   ~ 0
cD12
Text Label 1750 4450 0    60   ~ 0
cD13
Text Label 1750 5050 0    60   ~ 0
cD14
Text Label 1750 4350 0    60   ~ 0
cD15
$Comp
L GND #PWR01
U 1 1 5606DE79
P 2700 2150
F 0 "#PWR01" H 2700 1900 50  0001 C CNN
F 1 "GND" H 2700 2000 50  0000 C CNN
F 2 "" H 2700 2150 60  0000 C CNN
F 3 "" H 2700 2150 60  0000 C CNN
	1    2700 2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5606DE91
P 2950 3900
F 0 "#PWR02" H 2950 3650 50  0001 C CNN
F 1 "GND" H 2950 3750 50  0000 C CNN
F 2 "" H 2950 3900 60  0000 C CNN
F 3 "" H 2950 3900 60  0000 C CNN
	1    2950 3900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5606DEA7
P 2350 5400
F 0 "#PWR03" H 2350 5150 50  0001 C CNN
F 1 "GND" H 2350 5250 50  0000 C CNN
F 2 "" H 2350 5400 60  0000 C CNN
F 3 "" H 2350 5400 60  0000 C CNN
	1    2350 5400
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR04
U 1 1 5606E023
P 2350 4150
F 0 "#PWR04" H 2350 4000 50  0001 C CNN
F 1 "+5V" H 2350 4290 50  0000 C CNN
F 2 "" H 2350 4150 60  0000 C CNN
F 3 "" H 2350 4150 60  0000 C CNN
	1    2350 4150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 5606E047
P 2900 2600
F 0 "#PWR05" H 2900 2450 50  0001 C CNN
F 1 "+5V" H 2900 2740 50  0000 C CNN
F 2 "" H 2900 2600 60  0000 C CNN
F 3 "" H 2900 2600 60  0000 C CNN
	1    2900 2600
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR06
U 1 1 5606E078
P 2700 850
F 0 "#PWR06" H 2700 700 50  0001 C CNN
F 1 "+5V" H 2700 990 50  0000 C CNN
F 2 "" H 2700 850 60  0000 C CNN
F 3 "" H 2700 850 60  0000 C CNN
	1    2700 850 
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C609
U 1 1 5606E313
P 2450 4200
F 0 "C609" H 2460 4270 50  0000 L CNN
F 1 "0.1" H 2460 4120 50  0000 L CNN
F 2 "" H 2450 4200 60  0001 C CNN
F 3 "" H 2450 4200 60  0000 C CNN
	1    2450 4200
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR07
U 1 1 5606E406
P 2550 4200
F 0 "#PWR07" H 2550 3950 50  0001 C CNN
F 1 "GND" H 2550 4050 50  0000 C CNN
F 2 "" H 2550 4200 60  0000 C CNN
F 3 "" H 2550 4200 60  0000 C CNN
	1    2550 4200
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR08
U 1 1 5606E4AA
P 2500 950
F 0 "#PWR08" H 2500 700 50  0001 C CNN
F 1 "GND" H 2500 800 50  0000 C CNN
F 2 "" H 2500 950 60  0000 C CNN
F 3 "" H 2500 950 60  0000 C CNN
	1    2500 950 
	0    1    1    0   
$EndComp
$Comp
L CP1_Small C608
U 1 1 5606E4D0
P 2600 950
F 0 "C608" H 2610 1020 50  0000 L CNN
F 1 ".1" H 2610 870 50  0000 L CNN
F 2 "" H 2600 950 60  0001 C CNN
F 3 "" H 2600 950 60  0000 C CNN
	1    2600 950 
	0    1    1    0   
$EndComp
Entry Wire Line
	3700 1200 3800 1100
Entry Wire Line
	3700 1300 3800 1200
Entry Wire Line
	3700 1400 3800 1300
Entry Wire Line
	3700 1500 3800 1400
Entry Wire Line
	3700 1600 3800 1500
Entry Wire Line
	3700 1700 3800 1600
Entry Wire Line
	3700 1800 3800 1700
Entry Wire Line
	3700 1900 3800 1800
Text HLabel 4300 1100 2    60   BiDi ~ 0
MUX_D0
Text HLabel 4300 1200 2    60   BiDi ~ 0
MUX_D1
Text HLabel 4300 1300 2    60   BiDi ~ 0
MUX_D2
Text HLabel 4300 1400 2    60   BiDi ~ 0
MUX_D3
Text HLabel 4300 1500 2    60   BiDi ~ 0
MUX_D4
Text HLabel 4300 1600 2    60   BiDi ~ 0
MUX_D5
Text HLabel 4300 1700 2    60   BiDi ~ 0
MUX_D6
Text HLabel 4300 1800 2    60   BiDi ~ 0
MUX_D7
Text Label 3900 1100 0    60   ~ 0
oD0
Text Label 3900 1200 0    60   ~ 0
oD1
Text Label 3900 1300 0    60   ~ 0
oD2
Text Label 3900 1400 0    60   ~ 0
oD3
Text Label 3900 1500 0    60   ~ 0
oD4
Text Label 3900 1600 0    60   ~ 0
oD5
Text Label 3900 1700 0    60   ~ 0
oD6
Text Label 3900 1800 0    60   ~ 0
oD7
Text HLabel 1050 2750 0    60   Input ~ 0
U602-2
$Comp
L 74LS04 U602
U 5 1 5606F172
P 4750 2400
F 0 "U602" H 4945 2515 60  0000 C CNN
F 1 "74LS04" H 4940 2275 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 4750 2400 60  0001 C CNN
F 3 "" H 4750 2400 60  0000 C CNN
	5    4750 2400
	1    0    0    -1  
$EndComp
Text HLabel 6400 2400 2    60   Output ~ 0
U602-10
Text HLabel 10700 3600 2    60   Output ~ 0
U602-8
$Comp
L 74LS04 U602
U 4 1 560B404A
P 9900 3600
F 0 "U602" H 10095 3715 60  0000 C CNN
F 1 "74LS04" H 10090 3475 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 9900 3600 60  0001 C CNN
F 3 "" H 9900 3600 60  0000 C CNN
	4    9900 3600
	1    0    0    -1  
$EndComp
Text Label 10650 3600 2    60   ~ 0
A15
$Comp
L GND #PWR09
U 1 1 560B4793
P 6500 3400
F 0 "#PWR09" H 6500 3150 50  0001 C CNN
F 1 "GND" H 6500 3250 50  0000 C CNN
F 2 "" H 6500 3400 60  0000 C CNN
F 3 "" H 6500 3400 60  0000 C CNN
	1    6500 3400
	1    0    0    -1  
$EndComp
Text HLabel 4800 4350 0    60   Input ~ 0
U601-14
Text Label 4850 4350 0    60   ~ 0
~Ø1
Text Label 1150 2750 0    60   ~ 0
~DBIN
Text HLabel 8000 1500 0    60   Input ~ 0
U600-61
Text HLabel 10700 1600 2    60   Output ~ 0
U606-3
$Comp
L 74LS04 U602
U 3 1 5614248B
P 8700 1500
F 0 "U602" H 8895 1615 60  0000 C CNN
F 1 "74LS04" H 8890 1375 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 8700 1500 60  0001 C CNN
F 3 "" H 8700 1500 60  0000 C CNN
	3    8700 1500
	1    0    0    -1  
$EndComp
Text Label 8050 1500 0    60   ~ 0
~WE
$Comp
L 74LS00 U606
U 1 1 561425C1
P 9750 1600
F 0 "U606" H 9750 1650 60  0000 C CNN
F 1 "74LS00" H 9750 1500 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 9750 1600 60  0001 C CNN
F 3 "" H 9750 1600 60  0000 C CNN
	1    9750 1600
	1    0    0    1   
$EndComp
Text Label 10650 1600 2    60   ~ 0
~mWE
$Comp
L 74LS74 U607
U 1 1 561429F8
P 8900 2450
F 0 "U607" H 9200 2750 60  0000 R CNN
F 1 "74LS74" H 9200 2155 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 8900 2450 60  0001 C CNN
F 3 "" H 8900 2450 60  0000 C CNN
	1    8900 2450
	1    0    0    -1  
$EndComp
Text HLabel 7600 2450 0    60   Input ~ 0
U601-6
Text Label 7650 2450 0    60   ~ 0
~Ø4
$Comp
L 74LS74 U607
U 2 1 5609DB20
P 5600 3300
F 0 "U607" H 5900 3600 60  0000 R CNN
F 1 "74LS74" H 5900 3005 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 5600 3300 60  0001 C CNN
F 3 "" H 5600 3300 60  0000 C CNN
	2    5600 3300
	1    0    0    1   
$EndComp
$Comp
L +5V #PWR010
U 1 1 5609DFB7
P 5600 2750
F 0 "#PWR010" H 5600 2600 50  0001 C CNN
F 1 "+5V" H 5600 2890 50  0000 C CNN
F 2 "" H 5600 2750 60  0000 C CNN
F 3 "" H 5600 2750 60  0000 C CNN
	1    5600 2750
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR011
U 1 1 5609DFEF
P 5600 3850
F 0 "#PWR011" H 5600 3700 50  0001 C CNN
F 1 "+5V" H 5600 3990 50  0000 C CNN
F 2 "" H 5600 3850 60  0000 C CNN
F 3 "" H 5600 3850 60  0000 C CNN
	1    5600 3850
	-1   0    0    1   
$EndComp
$Comp
L +5V #PWR012
U 1 1 5609E320
P 8900 3000
F 0 "#PWR012" H 8900 2850 50  0001 C CNN
F 1 "+5V" H 8900 3140 50  0000 C CNN
F 2 "" H 8900 3000 60  0000 C CNN
F 3 "" H 8900 3000 60  0000 C CNN
	1    8900 3000
	-1   0    0    1   
$EndComp
Text HLabel 4800 3300 0    60   Input ~ 0
U601-15
Text Label 4850 3300 0    60   ~ 0
~Ø2
Text HLabel 4450 3500 0    60   Input ~ 0
U506-3
Text Label 4500 3500 0    60   ~ 0
READY/~HOLD
$Comp
L 74LS00 U603
U 3 1 560D22AB
P 2650 5800
F 0 "U603" H 2650 5850 60  0000 C CNN
F 1 "74LS00" H 2650 5700 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 2650 5800 60  0001 C CNN
F 3 "" H 2650 5800 60  0000 C CNN
	3    2650 5800
	-1   0    0    -1  
$EndComp
$Comp
L 74LS00 U603
U 2 1 560D2DE5
P 4800 4650
F 0 "U603" H 4800 4700 60  0000 C CNN
F 1 "74LS00" H 4800 4550 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 4800 4650 60  0001 C CNN
F 3 "" H 4800 4650 60  0000 C CNN
	2    4800 4650
	-1   0    0    -1  
$EndComp
$Comp
L 74LS00 U606
U 2 1 560D2E2D
P 4800 5100
F 0 "U606" H 4800 5150 60  0000 C CNN
F 1 "74LS00" H 4800 5000 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 4800 5100 60  0001 C CNN
F 3 "" H 4800 5100 60  0000 C CNN
	2    4800 5100
	-1   0    0    -1  
$EndComp
Text HLabel 5850 5200 2    60   Input ~ 0
U505-13
Text Label 5800 5200 2    60   ~ 0
~VDPR_EN
$Comp
L 74LS00 U603
U 4 1 560D44F4
P 8750 5050
F 0 "U603" H 8750 5100 60  0000 C CNN
F 1 "74LS00" H 8750 4950 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 8750 5050 60  0001 C CNN
F 3 "" H 8750 5050 60  0000 C CNN
	4    8750 5050
	1    0    0    1   
$EndComp
Text HLabel 10700 5050 2    60   Output ~ 0
U600-62
$Comp
L 74LS04 U602
U 2 1 560D47E0
P 9900 5050
F 0 "U602" H 10095 5165 60  0000 C CNN
F 1 "74LS04" H 10090 4925 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 9900 5050 60  0001 C CNN
F 3 "" H 9900 5050 60  0000 C CNN
	2    9900 5050
	1    0    0    -1  
$EndComp
Text Label 10650 5050 2    60   ~ 0
READY
$Comp
L 74LS00 U603
U 1 1 560D5BF0
P 7550 5400
F 0 "U603" H 7550 5450 60  0000 C CNN
F 1 "74LS00" H 7550 5300 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 7550 5400 60  0001 C CNN
F 3 "" H 7550 5400 60  0000 C CNN
	1    7550 5400
	1    0    0    1   
$EndComp
$Comp
L 74LS04 U604
U 1 1 560D5DFC
P 4850 6400
F 0 "U604" H 5045 6515 60  0000 C CNN
F 1 "74LS04" H 5040 6275 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 4850 6400 60  0001 C CNN
F 3 "" H 4850 6400 60  0000 C CNN
	1    4850 6400
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U612
U 1 1 560D65E2
P 8900 4000
F 0 "U612" H 8900 4050 60  0000 C CNN
F 1 "74LS00" H 8900 3900 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 8900 4000 60  0001 C CNN
F 3 "" H 8900 4000 60  0000 C CNN
	1    8900 4000
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR013
U 1 1 560D664B
P 8300 3800
F 0 "#PWR013" H 8300 3650 50  0001 C CNN
F 1 "+5V" H 8300 3940 50  0000 C CNN
F 2 "" H 8300 3800 60  0000 C CNN
F 3 "" H 8300 3800 60  0000 C CNN
	1    8300 3800
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U612
U 2 1 560D6844
P 8900 4500
F 0 "U612" H 8900 4550 60  0000 C CNN
F 1 "74LS00" H 8900 4400 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 8900 4500 60  0001 C CNN
F 3 "" H 8900 4500 60  0000 C CNN
	2    8900 4500
	1    0    0    -1  
$EndComp
Text HLabel 1050 6200 0    60   Input ~ 0
U507-8
Text Label 1150 6200 0    60   ~ 0
~MB
Text Label 1150 6400 0    60   ~ 0
~ROMEN
Text HLabel 1050 6400 0    60   Input ~ 0
R527-2
$Comp
L 74LS00 U606
U 4 1 560D5935
P 2200 6300
F 0 "U606" H 2200 6350 60  0000 C CNN
F 1 "74LS00" H 2200 6200 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 2200 6300 60  0001 C CNN
F 3 "" H 2200 6300 60  0000 C CNN
	4    2200 6300
	1    0    0    -1  
$EndComp
Text HLabel 1050 6600 0    60   Input ~ 0
U605-3
Text Label 1150 6600 0    60   ~ 0
~MEMEN
$Comp
L 74LS32 U605
U 4 1 560D6048
P 3600 6400
F 0 "U605" H 3600 6450 60  0000 C CNN
F 1 "74LS32" H 3600 6350 60  0000 C CNN
F 2 "Sockets_DIP:DIP-14__300" H 3600 6400 60  0001 C CNN
F 3 "" H 3600 6400 60  0000 C CNN
	4    3600 6400
	1    0    0    1   
$EndComp
Wire Bus Line
	3700 1200 3700 4950
Wire Wire Line
	3400 1100 3600 1100
Wire Wire Line
	3600 1200 3400 1200
Wire Wire Line
	3400 1300 3600 1300
Wire Wire Line
	3600 1400 3400 1400
Wire Wire Line
	3400 1500 3600 1500
Wire Wire Line
	3600 1600 3400 1600
Wire Wire Line
	3400 1700 3600 1700
Wire Wire Line
	3600 1800 3400 1800
Wire Wire Line
	3350 2850 3600 2850
Wire Wire Line
	3600 2950 3350 2950
Wire Wire Line
	3350 3050 3600 3050
Wire Wire Line
	3600 3150 3350 3150
Wire Wire Line
	3350 3250 3600 3250
Wire Wire Line
	3600 3350 3350 3350
Wire Wire Line
	3350 3450 3600 3450
Wire Wire Line
	3600 3550 3350 3550
Wire Wire Line
	3350 4350 3600 4350
Wire Wire Line
	3600 4450 3350 4450
Wire Wire Line
	3350 4550 3600 4550
Wire Wire Line
	3600 4650 3350 4650
Wire Wire Line
	3350 4750 3600 4750
Wire Wire Line
	3600 4850 3350 4850
Wire Wire Line
	3350 4950 3600 4950
Wire Wire Line
	3600 5050 3350 5050
Wire Bus Line
	1600 1200 1600 4950
Wire Wire Line
	1700 2850 1950 2850
Wire Wire Line
	1950 2950 1700 2950
Wire Wire Line
	1700 3050 1950 3050
Wire Wire Line
	1950 3150 1700 3150
Wire Wire Line
	1700 3250 1950 3250
Wire Wire Line
	1950 3350 1700 3350
Wire Wire Line
	1700 3450 1950 3450
Wire Wire Line
	1950 3550 1700 3550
Wire Wire Line
	1700 4350 1950 4350
Wire Wire Line
	1950 4450 1700 4450
Wire Wire Line
	1700 4550 1950 4550
Wire Wire Line
	1950 4650 1700 4650
Wire Wire Line
	1700 4750 1950 4750
Wire Wire Line
	1950 4850 1700 4850
Wire Wire Line
	1700 4950 1950 4950
Wire Wire Line
	1950 5050 1700 5050
Wire Wire Line
	1050 1900 1500 1900
Wire Wire Line
	1500 2000 1050 2000
Wire Wire Line
	1050 2100 1500 2100
Wire Wire Line
	1500 2200 1050 2200
Wire Wire Line
	1050 2300 1500 2300
Wire Wire Line
	1500 2400 1050 2400
Wire Wire Line
	1050 2500 1500 2500
Wire Wire Line
	1500 2600 1050 2600
Wire Wire Line
	1050 1100 1500 1100
Wire Wire Line
	1500 1200 1050 1200
Wire Wire Line
	1050 1300 1500 1300
Wire Wire Line
	1500 1400 1050 1400
Wire Wire Line
	1050 1500 1500 1500
Wire Wire Line
	1500 1600 1050 1600
Wire Wire Line
	1050 1700 1500 1700
Wire Wire Line
	1500 1800 1050 1800
Wire Wire Line
	1700 1100 2000 1100
Wire Wire Line
	2000 1200 1700 1200
Wire Wire Line
	1700 1300 2000 1300
Wire Wire Line
	2000 1400 1700 1400
Wire Wire Line
	1700 1500 2000 1500
Wire Wire Line
	2000 1600 1700 1600
Wire Wire Line
	1700 1700 2000 1700
Wire Wire Line
	2000 1800 1700 1800
Wire Wire Line
	2900 2600 2900 2800
Wire Wire Line
	2350 4150 2350 4300
Connection ~ 2350 4200
Wire Wire Line
	2700 850  2700 1050
Connection ~ 2700 950 
Wire Wire Line
	3800 1100 4300 1100
Wire Wire Line
	4300 1200 3800 1200
Wire Wire Line
	3800 1300 4300 1300
Wire Wire Line
	4300 1400 3800 1400
Wire Wire Line
	3800 1500 4300 1500
Wire Wire Line
	4300 1600 3800 1600
Wire Wire Line
	3800 1700 4300 1700
Wire Wire Line
	4300 1800 3800 1800
Wire Wire Line
	1800 2750 1050 2750
Wire Wire Line
	1800 2000 1800 2750
Wire Wire Line
	1800 2000 2000 2000
Wire Wire Line
	1800 2400 4300 2400
Connection ~ 1800 2400
Wire Wire Line
	10350 3600 10700 3600
Wire Wire Line
	8000 3600 9450 3600
Wire Wire Line
	6600 3400 6600 3600
Connection ~ 6600 3500
Wire Wire Line
	6500 3400 6600 3400
Connection ~ 6600 3400
Wire Wire Line
	10400 5900 10400 3250
Wire Wire Line
	10400 3250 6400 3250
Wire Wire Line
	6400 3250 6400 3850
Wire Wire Line
	6400 3850 6600 3850
Connection ~ 10400 3600
Wire Wire Line
	6600 4350 4800 4350
Wire Wire Line
	8000 1500 8250 1500
Wire Wire Line
	10350 1600 10700 1600
Wire Wire Line
	9150 1700 9050 1700
Wire Wire Line
	9050 1700 9050 1850
Wire Wire Line
	9500 1850 9500 2250
Wire Wire Line
	9050 1850 9500 1850
Wire Wire Line
	8300 2250 8150 2250
Wire Wire Line
	8150 2250 8150 4750
Connection ~ 8150 3600
Wire Wire Line
	8000 3500 8100 3500
Wire Wire Line
	8100 3500 8100 1900
Wire Wire Line
	8100 1900 8900 1900
Wire Wire Line
	7600 2450 8300 2450
Wire Wire Line
	6200 3500 6200 4950
Wire Wire Line
	6200 4100 6600 4100
Wire Wire Line
	4800 3300 5000 3300
Wire Wire Line
	4450 3500 5000 3500
Wire Wire Line
	1950 5250 1850 5250
Wire Wire Line
	1850 5250 1850 5800
Wire Wire Line
	1850 5800 2050 5800
Wire Wire Line
	1950 5350 1850 5350
Connection ~ 1850 5350
Wire Wire Line
	3250 5900 10400 5900
Wire Wire Line
	3800 2400 3800 5700
Wire Wire Line
	3800 5700 3250 5700
Connection ~ 3800 2400
Wire Wire Line
	3350 3750 3900 3750
Wire Wire Line
	3900 3750 3900 5900
Connection ~ 3900 5900
Wire Wire Line
	3350 3850 4000 3850
Wire Wire Line
	4000 3850 4000 4650
Wire Wire Line
	4000 4650 4200 4650
Wire Wire Line
	2000 2100 2000 2350
Wire Wire Line
	2000 2350 4100 2350
Wire Wire Line
	4100 2350 4100 5100
Wire Wire Line
	4100 5100 4200 5100
Wire Wire Line
	5400 5000 5500 5000
Wire Wire Line
	5500 5000 5500 4750
Wire Wire Line
	8150 4750 5400 4750
Connection ~ 5500 4750
Wire Wire Line
	5200 2400 6400 2400
Wire Wire Line
	6300 2400 6300 4550
Wire Wire Line
	6300 4550 5400 4550
Connection ~ 6300 2400
Wire Wire Line
	5400 5200 5850 5200
Wire Wire Line
	6200 4950 8150 4950
Connection ~ 6200 4100
Wire Wire Line
	10350 5050 10700 5050
Wire Wire Line
	9350 5050 9450 5050
Wire Wire Line
	8150 5150 8150 5400
Wire Wire Line
	6550 4500 6550 6400
Wire Wire Line
	6600 4200 6450 4200
Wire Wire Line
	6450 4200 6450 6050
Wire Wire Line
	6550 5300 6950 5300
Connection ~ 6550 5300
Wire Wire Line
	8300 3800 8300 3900
Wire Wire Line
	8000 3400 8200 3400
Wire Wire Line
	8200 3400 8200 4100
Wire Wire Line
	8200 4100 8300 4100
Wire Wire Line
	9500 4000 9500 4250
Wire Wire Line
	9500 4250 8300 4250
Wire Wire Line
	8300 4250 8300 4400
Wire Wire Line
	8300 4600 8150 4600
Connection ~ 8150 4600
Wire Wire Line
	9500 4500 9500 5650
Wire Wire Line
	9500 5650 6950 5650
Wire Wire Line
	6950 5650 6950 5500
Wire Wire Line
	1050 6200 1600 6200
Wire Wire Line
	1600 6400 1050 6400
Wire Wire Line
	1050 6600 3000 6600
Wire Wire Line
	3000 6600 3000 6500
Wire Wire Line
	2800 6300 3000 6300
Wire Wire Line
	4200 6400 4400 6400
Wire Wire Line
	6450 6050 4300 6050
Wire Wire Line
	4300 6050 4300 6400
Connection ~ 4300 6400
Wire Wire Line
	6550 6400 5300 6400
$Comp
L 74LS194 U613
U 1 1 560B435B
P 7300 3950
F 0 "U613" H 7400 3950 60  0000 C CNN
F 1 "74LS194" H 7400 3800 60  0000 C CNN
F 2 "Sockets_DIP:DIP-16__300" H 7300 3950 60  0001 C CNN
F 3 "" H 7300 3950 60  0000 C CNN
	1    7300 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 4500 6550 4500
$EndSCHEMATC
